/*
DEVELOPER:  Harry Staley
DATABASE:   Battle_Tome
CREATED:    2-17-2019
REVISED:    3-14-2019
*/

-- CREATE TABLES

--CREATE USERS TABLE

CREATE TABLE user_roles (
  id SERIAL PRIMARY KEY,
  role VARCHAR(20)
);

INSERT INTO user_roles
  (role)
VALUES
       ('ADMIN'),
       ('DM'),
       ('PLAYER');

-- note passwords are stored in a byte array format to support bcrypt.

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  email VARCHAR(255) NOT NULL UNIQUE ,
  role_id INT REFERENCES user_roles (id),
  password BYTEA ,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO users
  (email, role_id, password)
VALUES
       ('artem_skitenko@paladinsinhell.com', 1, 'password1'),
       ('harry_staley@paladinsinhell.com', 1, 'password2'),
       ('david_velez@paladinsinhell.com', 2, 'password3'),
       ('test_user@test.com', 3, 'password4');

-- CREATE CAMPAIGNS TABLE

CREATE TABLE campaigns (
  id SERIAL PRIMARY KEY,
  user_created_id INT REFERENCES users (id),
  name VARCHAR(255) NOT NULL UNIQUE ,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO campaigns
  (user_created_id, name)
VALUES
       (1, 'Enchanted Laptop'),
       (3, 'Ahktar''s Lair'),
       (2, 'Sands of Quatar'),
       (2, 'Hot Swamps'),
       (3, 'Hilltop Trail');

-- CREATE ENTITIES TABLE

CREATE TABLE entity_types (
  id SERIAL PRIMARY KEY,
  ent_type VARCHAR(5)
);

INSERT INTO entity_types
  (ent_type)
VALUES
       ('CHAR'),
       ('NPC'),
       ('MON');

-- CREATE TABLE CONDITIONS

CREATE TABLE conditions (
  id SERIAL PRIMARY KEY,
  condition VARCHAR(25)
);

INSERT INTO conditions
  (condition)
VALUES
       ('NONE'),('Blinded'),('Charmed'),('Deafened'),('Exhaustion'),
       ('Frightened'),('Grappled'),('Incapacitated'),('Invisible'),
       ('Paralyzed'),('Petrified'),('Poisoned'),('Prone'),
       ('Restrained'),('Stunned'),('Unconscious');

CREATE TABLE entities (
  id SERIAL PRIMARY KEY,
  user_id INT REFERENCES users (id),
  name VARCHAR(255) NOT NULL ,
  ent_type_id INT REFERENCES entity_types (id),
  strength_base INT NOT NULL ,
  dexterity_base INT NOT NULL ,
  constitution_base INT NOT NULL ,
  intelligence_base INT NOT NULL ,
  wisdom_base INT NOT NULL ,
  charisma_base INT NOT NULL ,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

-- CREATE CHARACTERS TABLE

CREATE TABLE alignments (
  id SERIAL PRIMARY KEY ,
  alignment varchar(50)
);

INSERT INTO alignments
  (alignment)
VALUES
       ('Lawful Good'),
       ('Neutral Good'),
       ('Chaotic Good'),
       ('Lawful Neutral'),
       ('True Neutral'),
       ('Chaotic Neutral'),
       ('Lawful Evil'),
       ('Neutral Evil'),
       ('Chaotic Evil');

-- CREATE TABLE RACES

CREATE TABLE races (
  id SERIAL PRIMARY KEY,
  race VARCHAR(25)
);

INSERT INTO races
  (race)
VALUES
       ('Dragonborn'),
       ('Dwarf'),
       ('Elf'),
       ('Gnome'),
       ('Half Elf'),
       ('Halfling'),
       ('Half Orc'),
       ('Human'),
       ('Tiefling');

-- CREATE TABLE CLASSES

CREATE TABLE classes (
  id SERIAL PRIMARY KEY,
  class varchar(50)
);

INSERT INTO classes
  (class)
VALUES ('Barbarian'),
       ('Bard'),
       ('Cleric'),
       ('Druid'),
       ('Fighter'),
       ('Monk'),
       ('Paladin'),
       ('Ranger'),
       ('Rogue'),
       ('Sorcerer'),
       ('Warlock'),
       ('Wizard');

-- CREATE TABLE BACGROUNDS

CREATE TABLE backgrounds (
  id SERIAL PRIMARY KEY,
  background VARCHAR(25)
);

INSERT INTO backgrounds
  (background)
VALUES
       ('Far Traveler'),
       ('Noble'),
       ('Guild Artisan'),
       ('Waterdhavian Noble'),
       ('Outlander'),
       ('Urchin'),
       ('Entertainer'),
       ('Mercenary Veteran'),
       ('Clan Crafter'),
       ('Sage'),
       ('Knight of the Order'),
       ('Urban Bounty Hunter'),
       ('Cloistered Scholar'),
       ('Folk Hero'),
       ('Sailor'),
       ('Hermit'),
       ('City Watch'),
       ('Soldier'),
       ('Charlatan'),
       ('Criminal'),
       ('Acolyte'),
       ('Courtier'),
       ('Uthgardt Tribe Member'),
       ('Faction Agent'),
       ('Inheritor');

-- CREATE CHARACTERS IN DATABASE

CREATE TABLE Characters (
  id SERIAL PRIMARY KEY,
  entity_id INT REFERENCES entities (id) NOT NULL,
  race_id INT REFERENCES races (id) NOT NULL,
  class_id INT REFERENCES classes (id) NOT NULL,
  level INT NOT NULL,
  background_id INT REFERENCES backgrounds (id) NOT NULL,
  alignment_id INT REFERENCES alignments (id) NOT NULL,
  faith VARCHAR(50)
);

INSERT INTO entities
  (user_id, name, ent_type_id, strength_base, dexterity_base, constitution_base, intelligence_base, wisdom_base, charisma_base)
VALUES
       (1, 'Intel the Proctor', 1, 7,7,7,7,7,7), -- ent id 1
       (1, 'Corsair "The Mechanical" Key Keeper', 1, 5,5,5,5,5,5), -- ent id 2
       (2, 'Logi the Tech', 1, 6,6,6,6,6,6), -- ent id 3
       (2, 'Yeti the Blue', 1, 4,4,4,4,4,4); -- ent id 4

INSERT INTO characters
  (entity_id, race_id, class_id, level, background_id, alignment_id, faith)
VALUES
       (1, 3, 5, 7, 4, 3, NULL),
       (2, 2, 3, 5, 5, 1, 'Behomet'),
       (3, 7, 8, 6, 6, 7, NULL),
       (4, 1, 12, 4, 7, 5, NULL);

-- CREATE ENTITY CONDITION TABLE

CREATE TABLE entity_condition (
  id SERIAL PRIMARY KEY,
  entity_id INT REFERENCES entities (id) NOT NULL,
  condition_id INT REFERENCES conditions (id) DEFAULT 1
);

INSERT INTO entity_condition
(entity_id, condition_id)
VALUES  (1, 3),(1, 2),(3, 3),(2, 1),(4,1);




-- INSTER FEATS TABLE

CREATE TABLE feats (
  id SERIAL PRIMARY KEY,
  feat VARCHAR(50)
);

INSERT INTO feats
    (feat)
VALUES ('Alert'),
        ('Athlete'),
        ('Actor'),
        ('Charger'),
        ('Crossbow Expert'),
        ('Defensive Duelist'),
        ('Dual Wielder');

-- INSERT NPCS TABLE

CREATE TABLE npcs (
  id SERIAL PRIMARY KEY,
  entity_id INT REFERENCES entities (id) NOT NULL,
  race_id INT REFERENCES races (id) NOT NULL,
  class_id INT REFERENCES  classes (id) NOT NULL,
  level INT NOT NULL,
  background_id INT REFERENCES backgrounds (id) NOT NULL,
  alignment_id INT REFERENCES alignments (id) NOT NULL,
  faith VARCHAR(50),
  feat_id INT REFERENCES feats (id) NOT NULL
);

INSERT INTO entities
  (user_id, name, ent_type_id, strength_base, dexterity_base, constitution_base, intelligence_base, wisdom_base, charisma_base)
VALUES
       (1, 'Shop Keep', 2, 7,7,7,7,7,7), -- ent id 5
       (1, 'Guy on Street', 2, 5,5,5,5,5,5), -- ent id 6
       (2, 'Bar Tender', 2, 6,6,6,6,6,6), -- ent id 7
       (2, 'Computer Science Student', 2, 4,4,4,4,4,4); -- ent id 8

INSERT INTO entity_condition
(entity_id)
VALUES  (5),(6),(7),(8);


INSERT INTO npcs
  (entity_id, race_id, class_id, level, background_id, alignment_id, faith, feat_id)
VALUES
       (5, 3, 5, 7, 4, 3, NULL, 1),
       (6, 2, 3, 5, 5, 1, 'Bahamut', 2),
       (7, 7, 8, 6, 6, 7, NULL, 3),
       (8, 1, 12, 4, 7, 5, NULL, 4);

-- CREATE MONSTERS TABLE

CREATE TABLE mon_types (
  id SERIAL PRIMARY KEY,
  mon_type VARCHAR(25)
);

INSERT INTO mon_types
(mon_type)
VALUES ('Aberration'),
       ('Beast'),
       ('Celestial'),
       ('Construct'),
       ('Dragon'),
       ('Elemental'),
       ('Fey'),
       ('Fiend'),
       ('Giant'),
       ('Humanoid'),
       ('Monstrosity'),
       ('Ooze'),
       ('Plant'),
       ('Undead'),
       ('Unknown');

CREATE TABLE swarm_types (
  id SERIAL PRIMARY KEY,
  swarm_type VARCHAR(25)
);

INSERT INTO swarm_types
(swarm_type)
VALUES ('Aboleth'),('Acolyte'),('Adult Black Dragon'),('Adult Blue Dragon'),('Adult Brass Dragon'),
('Adult Bronze Dragon'),('Adult Copper Dragon'),('Adult Gold Dragon'),('Adult Green Dragon'),
('Adult Red Dragon'),('Adult Silver Dragon'),('Adult White Dragon'),('Air Elemental'),('Allosaurus'),
('Ancient Black Dragon'),('Ancient Blue Dragon'),('Ancient Brass Dragon'),('Ancient Bronze Dragon'),
('Ancient Copper Dragon'),('Ancient Gold Dragon'),('Ancient Green Dragon'),('Ancient Red Dragon'),
('Ancient Silver Dragon'),('Ancient White Dragon'),('Androsphinx'),('Animated Armor'),
('Ankheg'),('Ankylosaurus'),('Ape'),('Archmage'),('Assassin'),('Awakened Shrub'),
('Awakened Tree'),('Axe Beak'),('Azer'),('Baboon'),('Badger'),('Balor'),('Bandit'),
('Bandit Captain'),('Banshee'),('Barbed Devil'),('Basilisk'),('Bat'),('Bearded Devil'),('Behir'),('Berserker'),('Black Bear'),
('Black Dragon Wyrmling'),('Black Pudding'),('Blink Dog'),
('Blood Hawk'),('Blue Dragon Wyrmling'),('Boar'),('Bone Devil'),('Brass Dragon Wyrmling'),
('Bronze Dragon Wyrmling'),('Brown Bear'),('Bugbear'),('Bulette'),('Camel'),('Cat'),('Centaur'),
('Chain Devil'),('Chimera'),('Chuul'),('Clay Golem'),('Cloaker'),('Cloud Giant'),
('Cockatrice'),('Commoner'),('Constrictor Snake'),('Copper Dragon Wyrmling'),
('COPY_OF_Chuul'),('Couatl'),('Crab'),('Crocodile'),('Cult Fanatic'),
('Cultist'),('Cyclops'),('Darkmantle'),('Death Dog'),
('Deep Gnome (Svirfneblin)'),('Deer'),('Deva'),('Dire Wolf'),
('Djinni'),('Doppelganger'),('Draft Horse'),('Dragon Turtle'),('Dretch'),('Drider'),
('Drow'),('Druid'),('Dryad'),('Duergar'),('Dust Mephit'),('Eagle'),('Earth Elemental'),
('Efreeti'),('Elephant'),('Elk'),('Erinyes'),
('Ettercap'),('Ettin'),
('Fire Elemental'),('Fire Giant'),
('Flameskull'),('Flesh Golem'),
('Flying Snake'),('Flying Sword'),
('Frog'),('Frost Giant'),
('Gargoyle'),
('Gelatinous Cube'),
('Ghast'),
('Ghost'),('Ghoul'),
('Giant Ape'),
('Giant Badger'),
('Giant Bat'),
('Giant Boar'),
('Giant Centipede'),
('Giant Constrictor Snake'),
('Giant Crab'),
('Giant Crocodile'),
('Giant Eagle'),
('Giant Elk'),
('Giant Fire Beetle'),
('Giant Frog'),
('Giant Goat'),
('Giant Hyena'),
('Giant Lizard'),
('Giant Octopus'),
('Giant Owl'),
('Giant Poisonous Snake'),
('Giant Rat'),
('Giant Scorpion'),
('Giant Sea Horse'),
('Giant Shark'),
('Giant Spider'),
('Giant Toad'),
('Giant Vulture'),
('Giant Wasp'),
('Giant Weasel'),
('Giant Wolf Spider'),
('Gibbering Mouther'),
('Glabrezu'),
('Gladiator'),
('Gnoll'),
('Goat'),
('Goblin'),
('Gold Dragon Wyrmling'),
('Gorgon'),
('Gray Ooze'),
('Green Dragon Wyrmling'),
('Green Hag'),
('Grick'),
('Griffon'),
('Grimlock'),
('Guard'),
('Guardian Naga'),
('Gynosphinx'),
('Half-Red Dragon Veteran'),
('Harpy'),
('Hawk'),
('Hell Hound'),
('Hezrou'),
('Hill Giant'),
('Hippogriff'),
('Hobgoblin'),
('Homunculus'),
('Horned Devil'),
('Hunter Shark'),
('Hydra'),
('Hyena'),
('Ice Devil'),
('Ice Mephit'),
('Imp'),
('Invisible Stalker'),
('Iron Golem'),
('Jackal'),
('Killer Whale'),
('Knight'),
('Kobold'),
('Kraken'),
('Lamia'),
('Lemure'),
('Lich'),
('Lion'),
('Lizard'),
('Lizardfolk'),
('Mage'),
('Magma Mephit'),
('Magmin'),
('Mammoth'),
('Manticore'),
('Marilith'),
('Mastiff'),
('Medusa'),
('Merfolk'),
('Merrow'),
('Mimic'),
('Minotaur'),
('Minotaur Skeleton'),
('Mule'),
('Mummy'),
('Mummy Lord'),
('Nalfeshnee'),
('Night Hag'),
('Nightmare'),
('Noble'),
('Nothic'),
('Ochre Jelly'),
('Octopus'),
('Ogre'),
('Ogre Zombie'),
('Oni'),
('Orc'),
('Otyugh'),
('Owl'),
('Owlbear'),
('Panther'),
('Pegasus'),
('Phase Spider'),
('Pit Fiend'),
('Planetar'),
('Plesiosaurus'),
('Poisonous Snake'),
('Polar Bear'),
('Pony'),
('Priest'),
('Pseudodragon'),
('Pteranodon'),
('Purple Worm'),
('Quasit'),
('Quipper'),
('Rakshasa'),
('Rat'),
('Raven'),
('Red Dragon Wyrmling'),
('Reef Shark'),
('Remorhaz'),
('Rhinoceros'),
('Riding Horse'),
('Roc'),
('Roper'),
('Rug of Smothering'),
('Rust Monster'),
('Saber-Toothed Tiger'),
('Sahuagin'),
('Salamander'),
('Satyr'),
('Scorpion'),
('Scout'),
('Sea Hag'),
('Sea Horse'),
('Shadow'),
('Shambling Mound'),
('Shield Guardian'),
('Shrieker'),
('Silver Dragon Wyrmling'),
('Skeleton'),
('Solar'),
('Spectator'),
('Specter'),
('Spider'),
('Spirit Naga'),
('Sprite'),
('Spy'),
('Steam Mephit'),
('Stirge'),
('Stone Giant'),
('Stone Golem'),
('Storm Giant'),
('Succubus/Incubus'),
('Swarm of Bats'),
('Swarm of Insects'),
('Swarm of Poisonous Snakes'),
('Swarm of Quippers'),
('Swarm of Rats'),
('Swarm of Ravens'),
('Tarrasque'),
('Thug'),
('Tiger'),
('Treant'),
('Tribal Warrior'),
('Triceratops'),
('Troll'),
('Twig Blight'),
('Tyrannosaurus Rex'),
('Unicorn'),
('Vampire'),
('Vampire Spawn'),
('Veteran'),
('Violet Fungus'),
('Vrock'),
('Vulture'),
('Warhorse'),
('Warhorse Skeleton'),
('Water Elemental'),
('Weasel'),
('Werebear'),
('Wereboar'),
('Wererat'),
('Weretiger'),
('Werewolf'),
('White Dragon Wyrmling'),
('Wight'),
('Will-o-Wisp'),
('Winter Wolf'),
('Wolf'),
('Worg'),
('Wraith'),
('Wyvern'),
('Xorn'),
('Yeti'),
('Young Black Dragon'),
('Young Blue Dragon'),
('Young Brass Dragon'),
('Young Bronze Dragon'),
('Young Copper Dragon'),
('Young Gold Dragon'),
('Young Green Dragon'),
('Young Red Dragon'),
('Young Silver Dragon'),
('Young White Dragon'),
('Zombie');

-- CREATE MONSTER SUB TYPES TABLE

CREATE TABLE mon_sub_types (
  id SERIAL PRIMARY KEY,
  mon_sub_type VARCHAR(25)
);

INSERT INTO mon_sub_types
(mon_sub_type)
VALUES  ('Angel'),('Any race'),('Bullywug'),('Cloud giant'),
        ('Demon'),('Derro'),('Devil'),('Dwarf'),
        ('Elf'),('Fire giant'),('Firenewt'),('Frost giant'),
        ('Gith'),('Gnoll'),('Gnome'),('Goblinoid'),
        ('Grimlock'),('Grung'),('Half-dragon'),('Half-elf'),
        ('Hill giant'),('Human'),('Inevitable'),('Kenku'),
        ('Kobold'),('Kraul'),('Kuo-toa'),('Lizardfolk'),
        ('Meazel'),('Merfolk'),('Nagpa'),('Orc'),
        ('Quaggoth'),('Sahuagin'),('Saurial'),('Shapechanger'),
        ('Simic hybrid'),('Stone giant'),('Storm giant'),('Ttabaxi'),
        ('Thri-keen'),('Titan'),('Tortle'),('Troglodyte'),
        ('Xvart'),('Yuan-ti'),('Yugoloth');

-- CREATE SIZES TABLE

CREATE TABLE sizes (
  id SERIAL PRIMARY KEY,
  size VARCHAR(25)
);

INSERT INTO sizes (size)
VALUES  ('Gargantuan'),('Huge'),('Large'),('Medium'),('Small'),('Tiny');

-- CREATE MONSTERS TABLE

CREATE TABLE monsters (
  id SERIAL PRIMARY KEY,
  entity_id INT REFERENCES entities (id) NOT NULL,
  type_id INT REFERENCES mon_types (id) NOT NULL,
  sub_type_id INT REFERENCES mon_sub_types (id) NOT NULL,
  swarm_type_id INT REFERENCES swarm_types (id) NOT NULL,
  size_id INT REFERENCES sizes (id) NOT NULL,
  alignment_id INT REFERENCES alignments (id) NOT NULL,
  challenge_rating INT NOT NULL,
  faith VARCHAR(50),
  armor_class INT NOT NULL,
  armor_class_type CHAR(25) NOT NULL
);

INSERT INTO entities
  (user_id, name, ent_type_id, strength_base, dexterity_base, constitution_base, intelligence_base, wisdom_base, charisma_base)
VALUES
       (1, 'Computer Science Professor', 3, 7,7,7,7,7,7), -- ent id 9
       (1, 'Academic Advisor', 3, 5,5,5,5,5,5), -- ent id 10
       (2, 'Crappy Boss', 3, 6,6,6,6,6,6), -- ent id 11
       (2, 'Pregnant Wife', 3, 4,4,4,4,4,4); -- ent id 12

INSERT INTO entity_condition
(entity_id)
VALUES  (9),(10),(11),(12);

INSERT INTO monsters
  (entity_id,
  type_id,
  sub_type_id,
  swarm_type_id,
  size_id,
  alignment_id,
  challenge_rating,
  faith,
  armor_class,
  armor_class_type)
VALUES
       (9 ,3,5 ,7, 4, 3, 5, 'Bubble God',          2, 'Skin'),
       (10,2,3 ,5, 5, 1, 4, 'Gozer Worshipers', 4, 'Scales'),
       (11,7,8 ,6, 6, 7, 3, 'Kali', 5, 'bone'),
       (12,1,12,4, 3, 5, 7, 'Spaghetti Monster', 2, 'fire');

-- CREATE BATTLES TABLE

CREATE TABLE battles (
  id SERIAL PRIMARY KEY,
  campaign_id INT REFERENCES campaigns (id) NOT NULL,
  name VARCHAR(100) NOT NULL,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
  last_acc_dt TIMESTAMP,
  complete_dt TIMESTAMP
);

INSERT INTO battles
  (campaign_id, name)
VALUES (1, 'RAM Battle'),
        (1, 'Holy Harddrive'),(1, 'Liquid Crystal Haunting'),
        (2, 'SOLID Arena'),(2, 'The Infinite Factory'),
        (2, 'Gang of Four Fight'),(2, 'Temple of Reflection'),
        (3, 'Ocean of Fire'),(3, 'Swords in the Sand'),
        (4, 'Born in the Bayou'),(4, 'The Lizard King'),
        (4, 'Bloody Baroness'),(4, 'The Boiling Bog'),
        (4, 'Alligator Bites'),(5, 'Assault at Jagged Teeth'),
        (5, 'Hill Giant Grudge'),(5, 'Dinner with the Mountain Hag'),
        (5, 'The Endless Tunnel'),(5, 'Orc Patrol'),
        (5, 'Rumble in the Jungle');

-- CREATE ACTIONS TABLE

CREATE TABLE action_types (
  id SERIAL PRIMARY KEY,
  action_type VARCHAR(25)
);

INSERT INTO action_types
  (action_type)
VALUES
       ('Attack'),
       ('Delay'),('Disengage'),('Dash'),('Dodge'),('Help'),('Hide'),('Ready'),('Search'),('Spell'),('Use an Object'),('Use a Skill');

-- CREATE TURNS TABLE

CREATE TABLE turns (
  id SERIAL PRIMARY KEY,
  battle_id INT REFERENCES battles (id) NOT NULL,
  attacker_id INT REFERENCES entities (id) NOT NULL,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO turns
        (battle_id, attacker_id)
VALUES
        (1, 1),
        (1, 2),(1, 3);

-- CREATE ACTIONS TABLE

CREATE TABLE actions (
  id SERIAL PRIMARY KEY,
  turn_id INT REFERENCES turns (id) NOT NULL,
  action_type_id INT REFERENCES action_types (id) NOT NULL,
  created_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO actions
        (turn_id, action_type_id)
VALUES
        (1,1),(1,6);

-- INSERT ACTION TARGET TABLE

CREATE TABLE action_target (
    id SERIAL PRIMARY KEY,
  action_id INT REFERENCES actions (id) NOT NULL,
  target_id INT REFERENCES entities (id) DEFAULT NULL
);

INSERT INTO action_target
(action_id, target_id)
VALUES  (1, 1),(1, 2),(1, 3),
        (2, 3),(2, 5);

-- INSERT DMG TYPE TABLE

CREATE TABLE dmg_types (
  id SERIAL PRIMARY KEY,
  dmg_type varchar(25)
);

INSERT INTO dmg_types
  (dmg_type)
VALUES
       ('Acid'),
       ('Bludgeoning'), ('Cold'),('Fire'),('Force'),
       ('Healing'),('Lightning'),('Necrotic'),('Piercing'),
       ('Poison'),('Psychic'),('Radiant'),('Slashing'),
       ('Thunder');

CREATE TABLE dmg (
  id SERIAL PRIMARY KEY,
  action_target_id INT REFERENCES action_target (id),
  type_id INT REFERENCES dmg_types (id) NOT NULL,
  value INT DEFAULT 0
);

INSERT INTO dmg
(action_target_id, type_id, value)
VALUES  (1, 2, 3),
        (2, 1, 5);

-- CREATE CAMPAIGN ENTITY TABLE

CREATE TABLE campaign_entity (
  id SERIAL PRIMARY KEY,
  campaign_id INT REFERENCES campaigns (id),
  entity_id INT REFERENCES entities (id)
);

INSERT INTO campaign_entity
  (campaign_id, entity_id)
VALUES
       (1, 1),(1, 2),(1, 3),(1, 4),(2, 4),(2, 3);

-- CREATE BATTLE ENTITY TABLE

CREATE TABLE battle_entity (
  id SERIAL PRIMARY KEY,
  battle_id INT REFERENCES battles (id),
  entity_id INT REFERENCES entities (id),
  initiative INT
);

INSERT INTO battle_entity
  (battle_id, entity_id, initiative)
VALUES
       (1, 1, 4),(1, 2, 3),(1, 3, 6),(1, 4, 7),
       (2, 1, 10),(2, 2, 39),(2, 3, 20),(2, 4, 4);

-- CREATE SOME USEFUL VIEWS

CREATE OR REPLACE VIEW characters_view AS
  SELECT u.email As creator,
         ent.name AS name,
         et.ent_type AS type,
         r.race AS race,
         cls.class AS class,
         c.level AS level,
         ent.strength_base AS str,
         ent.dexterity_base AS dex,
         ent.constitution_base AS con,
         ent.intelligence_base AS int,
         ent.wisdom_base AS wis,
         ent.charisma_base AS chr,
         b.background AS background,
         a.alignment AS alignment,
         c.faith AS faith
  FROM entities AS ent
  INNER JOIN entity_types et ON ent.ent_type_id = et.id
  INNER JOIN users u ON ent.user_id = u.id
  INNER JOIN Characters C on ent.id = C.entity_id
  INNER JOIN races r on C.race_id = r.id
  INNER JOIN backgrounds b on C.background_id = b.id
  INNER JOIN classes cls on C.class_id = cls.id
  INNER JOIN alignments a on C.alignment_id = a.id
  WHERE et.ent_type = 'CHAR';

CREATE OR REPLACE VIEW npcs_view AS
SELECT u.email As creator,
         ent.name AS name,
         et.ent_type AS type,
         r.race AS race,
         cls.class AS class,
         n.level AS level,
         ent.strength_base AS str,
         ent.dexterity_base AS dex,
         ent.constitution_base AS con,
         ent.intelligence_base AS int,
         ent.wisdom_base AS wis,
         ent.charisma_base AS chr,
         b.background AS background,
         a.alignment AS alignment,
         n.faith AS faith
  FROM entities AS ent
  INNER JOIN entity_types et ON ent.ent_type_id = et.id
  INNER JOIN users u ON ent.user_id = u.id
  INNER JOIN npcs n on ent.id = n.entity_id
  INNER JOIN races r on n.race_id = r.id
  INNER JOIN backgrounds b on n.background_id = b.id
  INNER JOIN classes cls on n.class_id = cls.id
  INNER JOIN alignments a on n.alignment_id = a.id
  WHERE et.ent_type = 'NPC';


CREATE OR REPLACE VIEW battles_view AS
  SELECT c.name AS campaign,
         bat.name AS battle
  FROM battles AS bat
  INNER JOIN campaigns c on bat.campaign_id = c.id;

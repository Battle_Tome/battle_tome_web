# Using the Scripts

## Description
In order to spin up your vagrant machine properly, there are a couple of caveats.
- This script is used to setup your vagrant instance for development purposes:
  - FOR VUE/REST TESTING: keep in mind that it uses both bash scripts and ansible scripts to do the work. Bash to get the python packages on the vagrant machine to use the Ansible Deployement of our build on the vagrant guest machine to duplicate the test enviornment.
  - FOR REST TESTING: This uses shell scripts to set up the basic server and build the database and only test the RESTful API.

## Requirements
- You need the following installed on your local machine.
  - [Ansible](https://launchpad.net/~ansible/+archive/ubuntu/ansible)
  - [Vagrant](https://www.vagrantup.com/)
  - [virtualbox](https://www.virtualbox.org/)
- You will need to enter your passwordless SSH Key that is **associated** to your **GitLab** account
  - To generate SSH Keys, the command is: `ssh-keygen -t rsa`
  - On *GitLab*, click on your profile picture, go to *Settings > SSH Keys*
- `create_vagrant.sh` must have **your account as the owner of the file** and must be **executable**
  - `sudo chown $USER:$USER create_vagrant.sh` - In order to change permissions to your account
  - `chmod +x create_vagrant.sh` - To make it executable
- The other part is that the NPM packages, Vue CLI, and build is not done with the script

## To build the full site with vue to test the front emd UI/UX.
### On Local Machine
1. Change directory into scripts directory, i.e `cd scripts`
2. Run the script `create_vagrant.sh`
3. You will be prompted to enter the name of your **private** and **passwordless** ssh file
4. You will be prompted about choosing a network interface, choose the one that has an active internet connection
5. The shell script and ansible script will take care of everything, if there is a failure, usually the error messages are very helpful.
6. Change directory to the same directory level (filepath) as the Vagrant file, i.e. `cd ../`
7. Run `sudo vagrant ssh`

### On Vagrant Instance
8. `cd /srv/battle_tome_web/battle_tome_vue`
9. `npm install` - For reasons unknown, this needs to be run **TWICE**
10. `npm install -g @vue/cli`
11. `sudo npm run build`
12. `cp -r dist/* /var/www/paladinsinpurgatory/`
13. `sudo systemctl restart nginx`
14. `ifconfig` 

## Steps to test the RESTful API without the frontend running.
### On Local Machine
1. Change directory into scripts directory, i.e `cd scripts`
2. Run the vagrantfile `vagrant up`
3. The shell script will take care of everything, if there is a failure, usually the error messages are very helpful.
4. Run `vagrant ssh`

## Point of Contact
David Velez or Harry Staley

#!/bin/bash
################################################################################
# Script     : rebuild_vue_project.sh
# Author     : David Velez
# Date       : 03/03/19
# Description: Rebuild PaladinsinHell Vue Project and restart NGINX service
################################################################################

# Navigate to directory
cd /srv/battle_tome_web 

# Do a git pull
sudo git pull

# Navigate to project folder
cd battle_tome_vue

# Build Vue Project
sudo npm run build

# Navigate to dist folder
cd dist

# Copy contents to html directory
sudo cp -r * /var/www/html/

# Restart NGINX
sudo systemctl restart nginx

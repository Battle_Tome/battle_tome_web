#!/bin/bash
################################################################################
# Script     : create_vagrant.sh
# Author     : David Velez
# Date       : 04/09/19
# Description: Retrieves your private keys to use them in Vagrant Guest
#              Machine
################################################################################

# User Prompt
echo "What is the name of your private ssh file (i.e. id_rsa)?
NOTE: The key has to be passwordless (only meant for development vagrant machine)"
read SSH_FILE

# Retrieve Private Key
cd ../../
cp ~/.ssh/$SSH_FILE id_rsa
sudo vagrant up
rm -rf id_rsa
sudo vagrant ssh

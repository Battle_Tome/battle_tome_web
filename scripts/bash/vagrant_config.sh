#!/bin/bash
# Author: Harry Staley and David Velez

apt-get -qqy update
apt install postgresql postgresql-contrib python -y
apt-get -qqy install software-properties-common python3-pip
apt-get -qqy update
su postgres -c 'createuser -dRS vagrant'
su vagrant -c 'createdb'
su vagrant -c 'createdb battle_tome'
su vagrant -c 'psql battle_tome -f '$1'/Battle_Tome_DB_Create.sql'
su postgres -c 'createuser -es root'
su postgres -c 'psql -c "GRANT ALL PRIVILEGES ON DATABASE battle_tome TO root;"'

vagrantTip="[35m[1mThe shared directory is located at /vagrant\nTo access your shared files: cd /vagrant(B[m"
echo -e $vagrantTip > /etc/motd

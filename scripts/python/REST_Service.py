#!/bin/env python
# coding: utf-8
"""
This module is responsible for defining the RESTful API for
the Battle Tome Web App.
"""

import sqlalchemy
from flask import Flask, request, make_response
from flask import jsonify
import bcrypt
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError, DBAPIError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from werkzeug import secure_filename
import PyPDF2
import os
import models as mod

__author__ = "Harry Staley <staleyh@craftedtech.net>"
__version__ = "1.0"

# INSTANTIATE THE APP
app = Flask(__name__)
app.config.from_object(__name__)

base = declarative_base()

# CONNECT TO THE BATTLE TOME DATABASE
# Location of the database
db_uri = 'postgresql:///battle_tome'
# The engine which the Session will use for connection resources
db_eng = create_engine(db_uri)
# configure Session class with desired options
ses = sessionmaker()
# create a configured "Session" class
ses.configure(bind=db_eng)
# create a Session
ses = ses()

meta = sqlalchemy.MetaData(db_eng)

# ENABLE CORS
# CORS(app)
ALLOWED_EXTENSIONS = {'pdf'}


def allowed_extension(file_name):
    """

    :param file_name: The name of the file sent to the server.
    :type file_name: string
    :return: if the extension is allowed.
    """
    return '.' in file_name and file_name.rsplit('.', 1)[1].lower() \
           in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
@app.route('/api', methods=['GET', 'POST'])
@app.route('/api/', methods=['GET', 'POST'])
def get_instructions():
    """

    :return: Instructions on how to use the Battle Tome RESTful API.
    """
    return "Welcome to the Battle Tome restful API<br><br>" \
           "HTTP REQUEST TYPES FOR RESTFUL API<br>" \
           "GET - Retrieve an instance<br>" \
           "POST - Add a new instance<br>" \
           "PUT - Update an instance<br>" \
           "DELETE - Delete an instance<br><br>" \
           "Ex: /campaigns/ returns all campaigns.<br>" \
           "Ex: /campaigns/1 returns the campaign with capaign_id is 1."


@app.route('/api/campaigns/<int:campaign_id>/battles', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/battles/', methods=['GET'])
def get_campaign_battles(campaign_id):
    """

    :param campaign_id: The id of the campaign you are wanting to
     get the battles from.
    :type campaign_id: Integer
    :return: JSON result of querying for the battles from a single
     campaign based on campaign_id.
    """
    try:
        battles = ses.query(mod.Battle).filter_by(
            campaign_id=campaign_id).all()
        response = jsonify(Data=[b.serialize for b in battles])
        return make_response(response, 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles', methods=['GET'])
@app.route('/api/battles/', methods=['GET'])
def get_battles():
    """

    :return: JSON result from query for all battles.
    """
    try:
        battles = ses.query(mod.Battle).all()
        return make_response(jsonify(Data=[b.serialize for b in battles]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles/<int:battle_id>', methods=['GET'])
@app.route('/api/battles/<int:battle_id>/', methods=['GET'])
def get_battle(battle_id):
    """

    :param battle_id: The id of the battle to be displayed.
    :type battle_id: Integer
    :return: JSON result of querying for a battle from a single
     campaign based on campaign_id and battle_id.
    """
    try:
        battle = ses.query(mod.Battle).filter_by(id=battle_id).one()
        return make_response(jsonify(Data=battle.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles/<int:battle_id>/entities', methods=['GET'])
@app.route('/api/battles/<int:battle_id>/entities/', methods=['GET'])
def get_battle_entities(battle_id):
    """

    :param battle_id: The id of the battle you are wanting to
     get the entities from.
    :type battle_id: Integer
    :return: JSON result of querying for the entities from a single
     battle based on battle_id.
    """
    try:
        entities = ses.query(mod.BattleEntity).filter_by(
            battle_id=battle_id).all()
        return make_response(jsonify(
            Data=[e.serialize for e in entities]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles/<int:battle_id>/turns', methods=['GET'])
@app.route('/api/battles/<int:battle_id>/turns/', methods=['GET'])
def get_battle_turns(battle_id):
    """

    :param battle_id: The id of the battle to be displayed.
    :type battle_id: Integer
    :return: JSON result of querying for all turns from a single
     battle based on battle_id.
    """
    try:
        turns = ses.query(mod.Turn).filter_by(battle_id=battle_id).all()
        return make_response(jsonify(Data=[t.serialize for t in turns]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/battles', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/battles/', methods=['GET'])
def post_battles(campaign_id):
    """

    :param campaign_id: The id of the campaign you want to associate the
     battle with.
    :type campaign_id: Integer
    :return: Success or failure of insert into the battles table.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)
    battle = mod.Battle(
        campaign_id=campaign_id,
        name=json_data.get('name'),
        user_created_id=json_data.get('user_id')
    )
    ses.add(battle)
    ses.flush()
    if battle.id is None:
        ses.rollback()
        response = {'status': "Failed"}
        return make_response(jsonify(response), 405)

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles/<int:battle_id>/entities', methods=['POST'])
@app.route('/api/battles/<int:battle_id>/entities/', methods=['POST'])
def post_battle_entity(battle_id):
    """

    :param battle_id: The id of the battle you are wanting to
     insert the entity from.
    :type battle_id: Integer
    :return: Success or failure of insert into the BattleEntity table.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'status': 'Failed', 'reason': 'No data provided'}
        return make_response(jsonify(response), 400)

    entities = json_data.get('entities')
    for e in entities:
        battle_entity = mod.BattleEntity(
            battle_id=battle_id,
            entity_id=e.get('entity_id'),
            initiative=e.get('initiative')
        )
        ses.add(battle_entity)
    ses.flush()
    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/battles/<int:battle_id>/turns', methods=['POST'])
@app.route('/api/battles/<int:battle_id>/turns/', methods=['POST'])
def post_battle_turn(battle_id):
    """
     :param battle_id: The id of the battle to insert the turn into.
     :type battle_id: Integer
     :return: success of failure of the insertion of the turn into the Turns
      table.
     """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    turn = mod.Turn(
        battle_id=battle_id,
        attacker_id=json_data.get('attacker_id')
    )
    ses.add(turn)
    ses.flush()

    # Get the actions array from json data
    actions = json_data.get('actions')
    # Add each action to the actions table in the database transaction.
    for a in actions:
        action = mod.Action(
            turn_id=turn.id,
            action_type_id=a.get('action_type_id')
        )
        ses.add(action)
        ses.flush()
        # Get the targets array from json data
        targets = a.get('targets')
        # Add each target to the action_target table in the database
        #  transaction
        print(targets)
        for t in targets:
            action_target = mod.ActionTarget(
                action_id=action.id,
                target_id=t.get('target_id')
            )
            ses.add(action_target)
            ses.flush()
            # get the damage array from json data.
            dmgs = t.get('dmgs')
            # Add each damage to the dmg table in the database transaction
            print(dmgs)
            for d in dmgs:
                damage = mod.Dmg(
                    action_target_id=action_target.id,
                    type_id=d.get('dmg_type_id'),
                    value=d.get('dmg_value')
                )
                ses.add(damage)
                ses.flush()
            # Get the conditions array from the json data.
            conditions = t.get('conditions')
            # Add each condition to the entity_condition table to the
            #  database transaction.
            print(conditions)
            for c in conditions:
                condition = mod.EntityCondition(
                    entity_id=t.get('target_id'),
                    condition_id=c.get('condition_id')
                )
                ses.add(condition)
                ses.flush()

    # Attempt to commit all transacitons to the databaase otherwise fail and
    #  rollback all transactions.
    try:
        ses.commit()
        resp = {'status': "Success"}
        return make_response(jsonify(resp), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        resp = {'status': "Failed", 'reason': e}
        return make_response(jsonify(resp), 405)


@app.route('/api/campaigns', methods=['GET'])
@app.route('/api/campaigns/', methods=['GET'])
def get_campaigns():
    """

    :return: JSON result from query for all campaigns.
    """
    try:
        campaigns = ses.query(mod.Campaign).all()
        return make_response(jsonify(
            Data=[c.serialize for c in campaigns]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/', methods=['GET'])
def get_campaign(campaign_id):
    """

    :param campaign_id: The id of the campaign you are looking for.
    :type campaign_id: Integer
    :return: JSON result of querying for a single campaign based on
     campaign_id.
    """
    try:
        campaign = ses.query(mod.Campaign).filter_by(id=campaign_id).one()
        return make_response(jsonify(Data=campaign.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/battles/<int:battle_id>',
           methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/battles/<int:battle_id>/',
           methods=['GET'])
def get_campaign_battle(campaign_id, battle_id):
    """

    :param campaign_id: The id of the campaign you are wanting to
     get the battle from.
    :type campaign_id: Integer
    :param battle_id: The id of the battle to be displayed.
    :type battle_id: Integer
    :return: JSON result of querying for a battle from a single
     campaign based on campaign_id and battle_id.
    """
    try:
        battle = ses.query(mod.Battle).filter_by(campaign_id=campaign_id,
                                                 id=battle_id).one()
        return make_response(jsonify(Data=battle.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/characters', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/characters/', methods=['GET'])
def get_campaign_characters(campaign_id):
    """

    :param campaign_id: The id of the campaign that you are getting the
     characters for.
    :type campaign_id: Integer
    :return: JSON result from query for all characters assigned to  the
     specified campaign based on the campaign_id..
    """
    try:
        characters = ses.query(mod.Character).join(mod.Entity) \
            .join(mod.CampaignEntity).filter_by(campaign_id=campaign_id).all()
        return make_response(jsonify(
            Data=[c.serialize for c in characters]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/entities', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/entities/', methods=['GET'])
def get_campaign_entities(campaign_id):
    """

    :param campaign_id: The id of the campaign that you are getting the
     entities for.
    :type campaign_id: Integer
    :return: JSON result from query for all entities assigned to  the
     specified campaign based on the campaign_id.
    """
    try:
        entities = ses.query(mod.CampaignEntity).filter_by(
            campaign_id=campaign_id).all()
        return make_response(jsonify(
            Data=[e.serialize for e in entities]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/entities', methods=['POST'])
@app.route('/api/campaigns/<int:campaign_id>/entities/', methods=['POST'])
def post_campaign_entity(campaign_id):
    """

    :return: Success or failure of insert into the CampaignEntity table.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    campaign_entity = mod.CampaignEntity(
        entity_id=json_data.get('entity_id'),
        campaign_id=campaign_id
    )
    ses.add(campaign_entity)
    ses.flush()
    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/npcs', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/npcs/', methods=['GET'])
def get_campaign_npcs(campaign_id):
    """

    :param campaign_id: The id of the campaign that you are getting the
     npcs for.
    :type campaign_id: Integer
    :return: JSON result from query for all npcs assigned to the
     specified campaign based on the campaign_id..
    """
    try:
        characters = ses.query(mod.Npc).join(mod.Entity) \
            .join(mod.CampaignEntity).filter_by(campaign_id=campaign_id).all()
        return make_response(jsonify(Data=[c.serialize for c in characters])
                             , 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns/<int:campaign_id>/monsters', methods=['GET'])
@app.route('/api/campaigns/<int:campaign_id>/monsters/', methods=['GET'])
def get_campaign_monsters(campaign_id):
    """

    :param campaign_id: The id of the campaign that you are getting the
     monsters for.
    :type campaign_id: Integer
    :return: JSON result from query for all monsters assigned to  the
     specified campaign based on the campaign_id..
    """
    try:
        characters = ses.query(mod.Monster).join(mod.Entity) \
            .join(mod.CampaignEntity).filter_by(campaign_id=campaign_id).all()
        return make_response(jsonify(
            Data=[c.serialize for c in characters]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/campaigns', methods=['POST'])
@app.route('/api/campaigns/', methods=['POST'])
def post_campaigns():
    """

    :return: Success or failure of insert into the campaigns table.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    campaign = mod.Campaign(
        name=json_data.get('name'),
        user_created_id=json_data.get('user_id')
    )

    ses.add(campaign)
    ses.flush()

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/characters', methods=['GET'])
@app.route('/api/characters/', methods=['GET'])
def get_characters():
    """

    :return: JSON result from query for all characters.
    """
    try:
        characters = ses.query(mod.Character).join(mod.Entity).all()
        return make_response(jsonify(
            Data=[c.serialize for c in characters]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/characters/<int:entity_id>', methods=['GET'])
@app.route('/api/characters/<int:entity_id>/', methods=['GET'])
def get_character(entity_id):
    """

    :param entity_id: The entity_id of the character you are querying for.
    :type entity_id: Integer
    :return: JSON result from query for the character identified by the
     entity_id.
    """
    try:
        character = ses.query(mod.Character).join(
            mod.Entity).filter_by(id=entity_id).first()
        return make_response(jsonify(Data=character.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/characters', methods=['POST'])
@app.route('/api/characters/', methods=['POST'])
def post_characters():
    """

    :return: Success or failure of insert into the characters and entities
     tables.
    """

    character = ses.query(mod.Character).first()
    if character is None:
        response = {'status': "Failed", 'reason': "Character not found."}
        return make_response(jsonify(response), 405)

    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    entity = character.Entity(
        user_id=json_data.get('user_id'),
        name=json_data.get('name'),
        ent_type_id=1,
        strength_base=json_data.get('strength_base'),
        dexterity_base=json_data.get('dexterity_base'),
        constitution_base=json_data.get('constitution_base'),
        intelligence_base=json_data.get('intelligence_base'),
        wisdom_base=json_data.get('wisdom_base'),
        charisma_base=json_data.get('charisma_base')
    )
    ses.add(entity)
    # communicates CRUD operations as a transaction to the database.
    ses.flush()

    character = mod.Character(
        entity_id=entity.id,
        race_id=json_data.get('race_id'),
        class_id=json_data.get('class_id'),
        level=json_data.get('level'),
        background_id=json_data.get('background_id'),
        alignment_id=json_data.get('alignment_id'),
        faith=json_data.get('faith')
    )
    ses.add(character)
    ses.flush()

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/characters/<int:character_id>', methods=['PUT'])
@app.route('/api/characters/<int:character_id>/', methods=['PUT'])
def put_character(character_id):
    """

    :param character_id: The id of the character you want to update.
    :type character_id: Integer
    :return: Success or failure of update to the character.
    """
    character = ses.query(mod.Character).get(character_id)
    if character is None:
        response = {'status':'Failed', 'message': 'Character does not exist'}
        return make_response(jsonify(response), 400)

    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'status': 'Failed','message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    character.entity.user_id = json_data.get('user_id')
    character.entity.name = json_data.get('name')
    character.entity.strength_base = json_data.get('strength_base')
    character.entity.dexterity_base = json_data.get('dexterity_base')
    character.entity.constitution_base = json_data.get('constitution_base')
    character.entity.intelligence_base = json_data.get('intelligence_base')
    character.entity.wisdom_base = json_data.get('wisdom_base'),
    character.entity.charisma_base = json_data.get('charisma_base')
    character.race_id = json_data.get('race_id')
    character.class_id = json_data.get('class_id')
    character.level = json_data.get('level')
    character.background_id = json_data.get('background_id')
    character.alignment_id = json_data.get('alignment_id')
    character.faith = json_data.get('faith')

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/characters/pdf', methods=['POST'])
@app.route('/api/characters/pdf/', methods=['POST'])
def post_characters_pdf():
    """

    :return: Success or failure of insert into the characters and entities
     tables based on data imported from the PDF character sheet.
    """
    filename = None
    raw_pdf = None
    pages = None
    attributes = None
    print(request.files)
    if 'file' not in request.files:
        response = {"status": "Failed", 'reason': 'File not in request.'}
        return make_response(jsonify(response), 405)

    try:
        file = request.files['file']
        directory = '../../uploads/'
        if file and allowed_extension(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(directory, filename))
            raw_pdf = PyPDF2.PdfFileReader(open(
                os.path.join(directory, filename), "rb"))
            pages = raw_pdf.getNumPages()
            attributes = {'ALIGNMENT': 'Alignment', 'FAITH': 'Faith',
                          'CharacterName': 'CharacterName',
                          'CLASS  LEVEL': ['Class', 'Lvl'], 'RACE': 'Race',
                          'BACKGROUND': 'Background', 'STRmod': 'Strength',
                          'DEXmod ': 'Dexterity', 'CONmod': 'Constitution',
                          'INTmod': 'Intelligence', 'WISmod': 'Wisdom',
                          'CHamod': 'Charisma'}

        data = {}
        for page in range(pages):
            raw_page = raw_pdf.getPage(page)

            try:
                for annotation in raw_page['/Annots']:
                    attribute = annotation.getObject()['/T']
                    if attribute in attributes.keys():
                        if attribute == 'CLASS  LEVEL':
                            temp = annotation.getObject()['/V'].split(' ')
                            data[attributes['CLASS  LEVEL'][-1]] = temp[-1]
                            data[attributes['CLASS  LEVEL'][0]] = ' ' \
                                .join(temp[0:-1])
                        else:
                            data[attributes[attribute]] = \
                                annotation.getObject()['/V']
            except (SQLAlchemyError, DBAPIError) as e:
                os.remove(os.path.join(directory, filename))
                response = {'status': "Failed", 'reason': e}
                return make_response(jsonify(response), 405)

        os.remove(os.path.join(directory, filename))
        response = {'status': "Success", 'data': data}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        response = {'status': "Failed", 'reason': e}
        return make_response(response, 405)


@app.route('/api/entities', methods=['GET'])
@app.route('/api/entities/', methods=['GET'])
def get_entities():
    """

    :return: JSON result from query all of the entities.
    """
    try:
        entities = ses.query(mod.Entity).all()
        return make_response(jsonify(
            Data=[e.serialize for e in entities]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/dmg_types', methods=['GET'])
@app.route('/api/dmg_types/', methods=['GET'])
def get_dmg_types():
    """

    :return: JSON result from query all of the damage types.
    """
    try:
        qry_result = ses.query(mod.DmgType).all()
        return make_response(jsonify(
            Data=[e.serialize for e in qry_result]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/entities/<int:entity_id>', methods=['GET'])
@app.route('/api/entities/<int:entity_id>/', methods=['GET'])
def get_entity(entity_id):
    """

    :param entity_id: The id of the entity that you are getting the
     entities for.
    :type entity_id: Integer
    :return: JSON result from query all of the entities returning the entity
     specified entity based on the entity_id.
    """
    try:
        entity = ses.query(mod.Entity).filter_by(id=entity_id).first()
        return make_response(jsonify(Data=entity.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/monsters', methods=['GET'])
@app.route('/api/monsters/', methods=['GET'])
def get_monsters():
    """

    :return: JSON result from query for all monsters.
    """
    try:
        monsters = ses.query(mod.Monster).join(mod.Entity).all()
        return make_response(jsonify(
            Data=[m.serialize for m in monsters]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/monsters/<int:monster_id>', methods=['GET'])
@app.route('/api/monsters/<int:monster_id>/', methods=['GET'])
def get_monster(monster_id):
    """

    :param monster_id: The id of the monster you want to get from the query.
    :type monster_id: Integer
    :return: JSON result from query for a monster with teh specified
     monster_id.
    """
    try:
        monster = ses.query(mod.Monster).join(
            mod.Entity).filter_by(id=monster_id).first()
        return make_response(jsonify(Data=monster.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/monsters/<int:monster_id>', methods=['PUT'])
@app.route('/api/monsters/<int:monster_id>/', methods=['PUT'])
def put_monster(monster_id):
    """

    :param monster_id: The id of the monster you want to get from the query.
    :type monster_id: Integer
    :return: Success or failure of update into the monsters and entities
     tables.
    """
    monster = ses.query(mod.Monster).get(monster_id)
    if monster is None:
        response = {'status': 'Failed', 'message': 'Monster does not exist'}
        return make_response(jsonify(response), 400)

    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'status':'Failed', 'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    monster.entity.name = json_data.get('name')
    monster.entity.strength_base = json_data.get('strength_base')
    monster.entity.dexterity_base = json_data.get('dexterity_base')
    monster.entity.constitution_base = json_data.get('constitution_base')
    monster.entity.intelligence_base = json_data.get('intelligence_base')
    monster.entity.wisdom_base = json_data.get('wisdom_base')
    monster.entity.charisma_base = json_data.get('charisma_base')
    monster.type_id = json_data.get('type_id')
    monster.sub_type_id = json_data.get('sub_type_id')
    monster.swarm_type_id = json_data.get('swarm_type_id')
    monster.size_id = json_data.get('size_id')
    monster.alignment_id = json_data.get('alignment_id')
    monster.challenge_rating = json_data.get('challenge_rating')
    monster.faith = json_data.get('faith')
    monster.armor_class = json_data.get('armor_class')
    monster.armor_class_type = json_data.get('armor_class_type')

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/monsters', methods=['POST'])
@app.route('/api/monsters/', methods=['POST'])
def post_monsters():
    """

    :return: Success or failure of insert into the monsters and entities
     tables.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    entity = mod.Entity(
        user_id=json_data.get('user_id'),
        name=json_data.get('name'),
        ent_type_id=3,
        strength_base=json_data.get('strength_base'),
        dexterity_base=json_data.get('dexterity_base'),
        constitution_base=json_data.get('constitution_base'),
        intelligence_base=json_data.get('intelligence_base'),
        wisdom_base=json_data.get('wisdom_base'),
        charisma_base=json_data.get('charisma_base')
    )
    ses.add(entity)
    ses.flush()

    monster = mod.Monster(
        entity_id=entity.id,
        type_id=json_data.get('type_id'),
        sub_type_id=json_data.get('sub_type_id'),
        swarm_type_id=json_data.get('swarm_type_id'),
        size_id=json_data.get('size_id'),
        alignment_id=json_data.get('alignment_id'),
        challenge_rating=json_data.get('challenge_rating'),
        faith=json_data.get('faith'),
        armor_class=json_data.get('armor_class'),
        armor_class_type=json_data.get('armor_class_type')
    )
    ses.add(monster)
    ses.flush()

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/npcs', methods=['GET'])
@app.route('/api/npcs/', methods=['GET'])
def get_npcs():
    """

    :return: JSON result from query for all npcs.
    """
    try:
        npcs = ses.query(mod.Npc).join(mod.Entity).all()
        return make_response(jsonify(Data=[n.serialize for n in npcs]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/npcs/<int:npc_id>', methods=['GET'])
@app.route('/api/npcs/<int:npc_id>/', methods=['GET'])
def get_npc(npc_id):
    """

    :param npc_id: The id of the NPC that you what to query for.
    :type npc_id: Integer
    :return: JSON result from query for a npc with teh specified npc_id.
    """
    try:
        npc = ses.query(mod.Npc).join(mod.Entity).filter_by(id=npc_id).first()
        return make_response(jsonify(Data=npc.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/npcs/<int:npc_id>', methods=['PUT'])
@app.route('/api/npcs/<int:npc_id>/', methods=['PUT'])
def put_npc(npc_id):
    """

    :param npc_id: The id of the NPC that you what to update.
    :type npc_id: Integer
    :return: Success or failure of update into the npcs and entities
     tables.
    """
    npc = ses.query(mod.Npc).get(npc_id)
    if npc is None:
        response = {'status': 'Failed', 'message': 'Npc does not exist'}
        return make_response(jsonify(response), 400)

    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    npc.entity.name = json_data.get('name')
    npc.entity.strength_base = json_data.get('strength_base')
    npc.entity.dexterity_base = json_data.get('dexterity_base')
    npc.entity.constitution_base = json_data.get('constitution_base')
    npc.entity.intelligence_base = json_data.get('intelligence_base')
    npc.entity.wisdom_base = json_data.get('wisdom_base')
    npc.entity.charisma_base = json_data.get('charisma_base')
    npc.race_id = json_data.get('race_id')
    npc.class_id = json_data.get('class_id')
    npc.level = json_data.get('level')
    npc.background_id = json_data.get('background_id')
    npc.alignment_id = json_data.get('alignment_id')
    npc.faith = json_data.get('faith')
    npc.feat_id = json_data.get('feat_id')

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/npcs', methods=['POST'])
@app.route('/api/npcs/', methods=['POST'])
def post_npcs():
    """

    :return: Success or failure of insert into the npcs and entities
     tables.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    entity = mod.Entity(
        user_id=json_data.get('user_id'),
        name=json_data.get('name'),
        ent_type_id=2,
        strength_base=json_data.get('strength_base'),
        dexterity_base=json_data.get('dexterity_base'),
        constitution_base=json_data.get('constitution_base'),
        intelligence_base=json_data.get('intelligence_base'),
        wisdom_base=json_data.get('wisdom_base'),
        charisma_base=json_data.get('charisma_base')
    )
    ses.add(entity)
    ses.flush()

    npc = mod.Npc(
        entity_id=entity.id,
        race_id=json_data.get('race_id'),
        class_id=json_data.get('class_id'),
        level=json_data.get('level'),
        background_id=json_data.get('background_id'),
        alignment_id=json_data.get('alignment_id'),
        faith=json_data.get('faith'),
        feat_id=json_data.get('feat_id')
    )
    ses.add(npc)

    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/turns', methods=['GET'])
@app.route('/api/turns/', methods=['GET'])
def get_turns():
    """

    :return: JSON result of querying for all turns.
    """
    try:
        turns = ses.query(mod.Turn).all()
        return make_response(jsonify(Data=[t.serialize for t in turns]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/turns/<int:turn_id>', methods=['GET'])
@app.route('/api/turns/<int:turn_id>/', methods=['GET'])
def get_turn(turn_id):
    """

    :param turn_id: The id of the turn to be displayed.
    :type turn_id: Integer
    :return: JSON result of querying for a single turn based on turn_id..
    """
    try:
        turn = ses.query(mod.Turn).filter_by(id=turn_id).first()
        return make_response(jsonify(Data=turn.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/turns/<int:turn_id>/actions', methods=['GET'])
@app.route('/api/turns/<int:turn_id>/actions/', methods=['GET'])
def get_turn_actions(turn_id):
    """

    :param turn_id: The id of the turn to be queried.
    :type turn_id: Integer
    :return: JSON result of querying for all actions from a single
     turn based on turn_id.
    """
    try:
        actions = ses.query(mod.Action).filter_by(turn_id=turn_id).all()
        return make_response(jsonify(Data=[a.serialize for a in actions]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(e)
        resp = {'status': "Failed", 'reason': e}
        return make_response(jsonify(resp), 405)


@app.route('/api/users', methods=['GET'])
@app.route('/api/users/', methods=['GET'])
def get_users():
    """

    :return: JSON result from query for all users.
    """
    try:
        users = ses.query(mod.User).all()
        return make_response(jsonify(Data=[u.serialize for u in users]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/users/<int:user_id>', methods=['GET'])
@app.route('/api/users/<int:user_id>/', methods=['GET'])
def get_user(user_id):
    """

    :param user_id: The id of the user that you are querying for.
    :type user_id: Integer
    :return: JSON result from query for the user specified by the user_id.
    """
    try:
        user = ses.query(mod.User).filter_by(id=user_id).one()
        return make_response(jsonify(Data=user.serialize), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/users', methods=['POST'])
@app.route('/api/users/', methods=['POST'])
def post_users():
    """

    :return: Success or failure of insert into the users table.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    # Test to see if the any of the required fields was not sent and if so
    # return an error.
    email = json_data.get('email')
    print(email)
    role_id = json_data.get('role_id')
    print(role_id)
    password = json_data.get('password').encode('utf-8')
    print(password)
    if email is None or password is None or role_id is None:
        response = {'status': "Failed"}
        return make_response(jsonify(response), 405)

    # Test to see if the user already exists and if so return an error.
    user_exists = ses.query(mod.User).filter_by(email=json_data.get('email')) \
        .first()
    if user_exists:
        response = {'status': "Failed"}
        return make_response(jsonify(response), 405)

    user = mod.User(
        email=json_data.get('email'),
        role_id=json_data.get('role_id'),
        password=bcrypt.hashpw(password, bcrypt.gensalt(14))
    )
    ses.add(user)
    ses.flush()
    try:
        ses.commit()
        response = {'status': "Success"}
        return make_response(jsonify(response), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/login', methods=['POST'])
@app.route('/api/login/', methods=['POST'])
def post_user_login():
    """

    :return: Success or failure of authentication of the user.
    """
    # Get the json data
    json_data = request.get_json(force=True)
    if not json_data:
        response = {'message': 'No input data provided'}
        return make_response(jsonify(response), 400)

    # Test to see if the any of the required fields was not sent and if so
    # return an error.
    email = json_data.get('email'),
    password = json_data.get('password').encode('utf-8')
    if email is None or password is None:
        response = {'status': "Failed"}
        return make_response(jsonify(response), 405)

    # Test to see if the user already exists and if not return an error.
    user = ses.query(mod.User).filter_by(email=json_data.get('email')) \
        .first()
    if user is None:
        response = {'status': "Failed"}
        return make_response(jsonify(response), 405)

    stored_password = user.password

    try:
        # Authenticate the provided user to the system.
        if bcrypt.hashpw(password, stored_password) == stored_password:
            message = "{} Authentication Success!".format(email)
            print(message)
            response = {'status': "Success", "message": message}
            return make_response(jsonify(response), 200)
        else:
            message = "{} Authentication Failed!".format(email)
            print(message)
            response = {'status': "Failed", "message": message}
            return make_response(jsonify(response), 401)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


@app.route('/api/user_roles', methods=['GET'])
@app.route('/api/user_roles/', methods=['GET'])
def get_user_roles():
    """

    :return: JSON result from query all of the user roles.
    """
    try:
        qry_result = ses.query(mod.UserRoles).all()
        return make_response(jsonify(
            Data=[e.serialize for e in qry_result]), 201)
    except (SQLAlchemyError, DBAPIError) as e:
        print(str(e))
        ses.rollback()
        response = {'status': "Failed", 'reason': e}
        return make_response(jsonify(response), 405)


if __name__ == '__main__':
    # CONFIGURATION
    app.debug = True
    app.run(host='0.0.0.0', port=5000)

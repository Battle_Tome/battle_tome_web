#!/bin/env python
# coding: utf-8

from sqlalchemy import CHAR, Column, DateTime, ForeignKey, Integer, \
    LargeBinary, String, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

__author__ = "Harry Staley <staleyh@craftedtech.net>"
__version__ = "1.0"

Base = declarative_base()
metadata = Base.metadata


class Action(Base):
    __tablename__ = 'actions'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('actions_id_seq'::regclass)"))
    turn_id = Column(Integer, ForeignKey(u'turns.id'), nullable=False)
    action_type_id = Column(ForeignKey(u'action_types.id'), nullable=False)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))

    act_type = relationship(u'ActionType')
    turn = relationship(u'Turn')
    targets = relationship(u'ActionTarget')

    @property
    def serialize(self):
        """

        :return: Serialized data from Action table.
        """
        return {
            'id': self.id,
            'turn_id': self.turn_id,
            'attacker_id': self.turn.attacker_id,
            'action_type': self.act_type.action_type,
            'action_type_id': self.action_type_id,
            'created_dt': self.created_dt,
            'targets': [t.serialize for t in self.targets]
        }


class ActionTarget(Base):
    __tablename__ = 'action_target'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('action_target_id_seq'::regclass)"))
    action_id = Column(Integer, ForeignKey(u'actions.id'), nullable=False)
    target_id = Column(Integer, ForeignKey(u'entities.id'), nullable=False)

    action = relationship(u'Action')
    target = relationship(u'Entity')
    dmg = relationship(u'Dmg')

    @property
    def serialize(self):
        """

        :return: Serialized data from ActionTarget table.
        """
        return {
            'id': self.id,
            'action_id': self.action_id,
            'target_id': self.target_id,
            'target_name': self.target.name,
            'target': self.target.serialize,
            'dmg': [d.serialize for d in self.dmg]
        }


class ActionType(Base):
    __tablename__ = 'action_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('action_types_id_seq'::regclass)"))
    action_type = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from ActionType table.
        """
        return {
            'id': self.id,
            'action_type': self.action_type
        }


class Alignment(Base):
    __tablename__ = 'alignments'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('alignments_id_seq'::regclass)"))
    alignment = Column(String(50))

    @property
    def serialize(self):
        """

        :return: Serialized data from Alignment table.
        """
        return {
            'id': self.id,
            'alignment': self.alignment
        }


class Background(Base):
    __tablename__ = 'backgrounds'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('backgrounds_id_seq'::regclass)"))
    background = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from Background table.
        """
        return {
            'id': self.id,
            'background': self.background
        }


class Battle(Base):
    __tablename__ = 'battles'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('battles_id_seq'::regclass)"))
    campaign_id = Column(Integer, ForeignKey(u'campaigns.id'), nullable=False)
    name = Column(String(100), nullable=False)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))
    last_acc_dt = Column(DateTime)
    complete_dt = Column(DateTime)

    campaign = relationship(u'Campaign')

    @property
    def serialize(self):
        """

        :return: Serialized data from Battle table.
        """
        return {
            'id': self.id,
            'campaign_id': self.campaign_id,
            'name': self.name,
            'created_dt': self.created_dt,
            'last_acc_dt': self.last_acc_dt,
            'complete_dt': self.complete_dt,
            'campaign_name': self.campaign.name
        }


class BattleEntity(Base):
    __tablename__ = 'battle_entity'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('battle_entity_id_seq'::regclass)"))
    battle_id = Column(ForeignKey(u'battles.id'), nullable=False)
    entity_id = Column(ForeignKey(u'entities.id'), nullable=False)
    initiative = Column(Integer)

    battle = relationship(u'Battle')
    entity = relationship(u'Entity')

    @property
    def serialize(self):
        """

        :return: Serialized data from BattleEntity table.
        """
        return {
            'id': self.id,
            'battle_id': self.battle_id,
            'entity_id': self.entity_id,
            'initiative': self.initiative,
            'battle_name': self.battle.name,
            'entity_name': self.entity.name,
            'entity_type': self.entity.enttity_type.ent_type
        }


class Campaign(Base):
    __tablename__ = 'campaigns'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('campaigns_id_seq'::regclass)"))
    user_created_id = Column(ForeignKey(u'users.id'))
    name = Column(String(255), nullable=False, unique=True)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))

    user_created = relationship(u'User')

    @property
    def serialize(self):
        """

        :return: Serialized data from Campaign table.
        """
        return {
            'id': self.id,
            'user_created_id': self.user_created_id,
            'name': self.name,
            'created_dt': self.created_dt,
            'user_created': self.user_created.email
        }


class CampaignEntity(Base):
    __tablename__ = 'campaign_entity'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('campaign_entity_id_seq'::regclass)"))
    campaign_id = Column(ForeignKey(u'campaigns.id'), nullable=False)
    entity_id = Column(ForeignKey(u'entities.id'), nullable=False)

    campaign = relationship(u'Campaign')
    entity = relationship(u'Entity')

    @property
    def serialize(self):
        """

        :return: Serialized data from CampaignEntity table.
        """
        return {
            'id': self.id,
            'campaign_id': self.campaign_id,
            'entity_id': self.entity_id,
            'campaign_name': self.campaign.name,
            'entity': self.entity.serialize
        }


class Character(Base):
    __tablename__ = 'characters'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('characters_id_seq'::regclass)"))
    entity_id = Column(ForeignKey(u'entities.id'), nullable=False)
    race_id = Column(ForeignKey(u'races.id'), nullable=False)
    class_id = Column(ForeignKey(u'classes.id'), nullable=False)
    level = Column(Integer, nullable=False)
    background_id = Column(ForeignKey(u'backgrounds.id'), nullable=False)
    alignment_id = Column(ForeignKey(u'alignments.id'), nullable=False)
    faith = Column(String(50))

    char_alignment = relationship(u'Alignment')
    char_background = relationship(u'Background')
    char_class = relationship(u'Class')
    entity = relationship(u'Entity')
    char_race = relationship(u'Race')

    @property
    def serialize(self):
        """

        :return: Serialized data from Character table.
        """
        return {
            'id': self.id,
            'entity_id': self.entity_id,
            'race_id': self.race_id,
            'class_id': self.class_id,
            'level': self.level,
            'background_id': self.background_id,
            'alignment_id': self.alignment_id,
            'faith': self.faith,
            'alignment': self.char_alignment.alignment,
            'background': self.char_background.background,
            'class': self.char_class.ent_class,
            'entity': self.entity.serialize,
            'race': self.char_race.race
        }


class Class(Base):
    __tablename__ = 'classes'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('classes_id_seq'::regclass)"))
    ent_class = Column('class', String(50))

    @property
    def serialize(self):
        """

        :return: Serialized data from Class table.
        """
        return {
            'id': self.id,
            'class': self.ent_class
        }


class Condition(Base):
    __tablename__ = 'conditions'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('conditions_id_seq'::regclass)"))
    condition = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from Condition table.
        """
        return {
            'id': self.id,
            'condition': self.condition
        }


class Dmg(Base):
    __tablename__ = 'dmg'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('dmg_id_seq'::regclass)"))
    action_target_id = Column(Integer, ForeignKey(
        u'action_target.id'), nullable=False)
    type_id = Column(Integer, ForeignKey(u'dmg_types.id'), nullable=False)
    value = Column(Integer, nullable=False)

    dmg_type = relationship(u'DmgType')

    @property
    def serialize(self):
        """

        :return: Serialized data from Dmg table.
        """
        return {
            'id': self.id,
            'action_target_id': self.action_target_id,
            'type_id': self.type_id,
            'value': self.value,
            'type': self.dmg_type.dmg_type
        }


class DmgType(Base):
    __tablename__ = 'dmg_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('dmg_types_id_seq'::regclass)"))
    dmg_type = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from DmgType table.
        """
        return {
            'id': self.id,
            'dmg_type': self.dmg_type
        }


class Entity(Base):
    __tablename__ = 'entities'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('entities_id_seq'::regclass)"))
    user_id = Column(ForeignKey(u'users.id'), nullable=False)
    name = Column(String(255), nullable=False)
    ent_type_id = Column(ForeignKey(u'entity_types.id'))
    strength_base = Column(Integer, nullable=False)
    dexterity_base = Column(Integer, nullable=False)
    constitution_base = Column(Integer, nullable=False)
    intelligence_base = Column(Integer, nullable=False)
    wisdom_base = Column(Integer, nullable=False)
    charisma_base = Column(Integer, nullable=False)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))

    enttity_type = relationship(u'EntityType')
    user = relationship(u'User')
    cond = relationship(u'EntityCondition')

    @property
    def serialize(self):
        """

        :return: Serialized data from Entity table.
        """
        return {
            'id': self.id,
            'user_id': self.user_id,
            'name': self.name,
            'ent_type_id': self.ent_type_id,
            'strength_base': self.strength_base,
            'dexterity_base': self.dexterity_base,
            'constitution_base': self.constitution_base,
            'intelligence_base': self.intelligence_base,
            'wisdom_base': self.wisdom_base,
            'charisma_base': self.charisma_base,
            'created_dt': self.created_dt,
            'ent_type': self.enttity_type.ent_type,
            'user_created': self.user.email,
            'conditions': [c.cond.condition for c in self.cond]
        }


class EntityCondition(Base):
    __tablename__ = 'entity_condition'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('entity_condition_id_seq'::regclass)"))
    entity_id = Column(Integer, ForeignKey(u'entities.id'))
    condition_id = Column(Integer, ForeignKey(u'conditions.id'))

    entity = relationship(u'Entity')
    cond = relationship(u'Condition')

    @property
    def serialize(self):
        """

        :return: Serialized data from EntityCondition table.
        """
        return {
            'id': self.id,
            'entity_id': self.entity_id,
            'condition_id': self.condition_id,
            'entity_name': self.entity.name,
            'condition': self.cond.condition
        }


class EntityType(Base):
    __tablename__ = 'entity_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('entity_types_id_seq'::regclass)"))
    ent_type = Column(String(5))

    @property
    def serialize(self):
        """

        :return: Serialized data from EntityType table.
        """
        return {
            'id': self.id,
            'ent_type': self.ent_type
        }


class Feat(Base):
    __tablename__ = 'feats'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('feats_id_seq'::regclass)"))
    feat = Column(String(50))

    @property
    def serialize(self):
        """

        :return: Serialized data from Feat table.
        """
        return {
            'id': self.id,
            'feat': self.feat
        }


class Monster(Base):
    __tablename__ = 'monsters'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('monsters_id_seq'::regclass)"))
    entity_id = Column(ForeignKey(u'entities.id'), nullable=False)
    type_id = Column(ForeignKey(u'mon_types.id'), nullable=False)
    sub_type_id = Column(ForeignKey(u'mon_sub_types.id'), nullable=False)
    swarm_type_id = Column(ForeignKey(u'swarm_types.id'), nullable=False)
    size_id = Column(ForeignKey(u'sizes.id'), nullable=False)
    alignment_id = Column(ForeignKey(u'alignments.id'), nullable=False)
    challenge_rating = Column(Integer, nullable=False)
    faith = Column(String(50))
    armor_class = Column(Integer, nullable=False)
    armor_class_type = Column(CHAR(25), nullable=False)

    mon_alignment = relationship(u'Alignment')
    entity = relationship(u'Entity')
    mon_size = relationship(u'Size')
    mon_sub_type = relationship(u'MonSubType')
    mon_swarm_type = relationship(u'SwarmType')
    mon_type = relationship(u'MonType')

    @property
    def serialize(self):
        """

        :return: Serialized data from Monster table.
        """
        return {
            'id': self.id,
            'entity_id': self.entity_id,
            'type_id': self.type_id,
            'sub_type_id': self.sub_type_id,
            'swarm_type_id': self.swarm_type_id,
            'size_id': self.size_id,
            'alignment_id': self.alignment_id,
            'challenge_rating': self.challenge_rating,
            'faith': self.faith,
            'armor_class': self.armor_class,
            'armor_class_type': self.armor_class_type,
            'alignment': self.mon_alignment.alignment,
            'entity': self.entity.serialize,
            'size': self.mon_size.size,
            'sub_type': self.mon_sub_type.mon_sub_type,
            'swarm_type': self.mon_swarm_type.swarm_type,
            'type': self.mon_type.mon_type
        }


class MonSubType(Base):
    __tablename__ = 'mon_sub_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('mon_sub_types_id_seq'::regclass)"))
    mon_sub_type = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from MonSubType table.
        """
        return {
            'id': self.id,
            'mon_syb_type': self.mon_sub_type
        }


class MonType(Base):
    __tablename__ = 'mon_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('mon_types_id_seq'::regclass)"))
    mon_type = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from MonType table.
        """
        return {
            'id': self.id,
            'mon_type': self.mon_type
        }


class Npc(Base):
    __tablename__ = 'npcs'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('npcs_id_seq'::regclass)"))
    entity_id = Column(ForeignKey(u'entities.id'), nullable=False)
    race_id = Column(ForeignKey(u'races.id'), nullable=False)
    class_id = Column(ForeignKey(u'classes.id'), nullable=False)
    level = Column(Integer, nullable=False)
    background_id = Column(ForeignKey(u'backgrounds.id'), nullable=False)
    alignment_id = Column(ForeignKey(u'alignments.id'), nullable=False)
    faith = Column(String(50))
    feat_id = Column(ForeignKey(u'feats.id'), nullable=False)

    npc_alignment = relationship(u'Alignment')
    npc_background = relationship(u'Background')
    npc_class = relationship(u'Class')
    entity = relationship(u'Entity')
    npc_feat = relationship(u'Feat')
    npc_race = relationship(u'Race')

    @property
    def serialize(self):
        """

        :return: Serialized data from NPC table.
        """
        return {
            'id': self.id,
            'entity_id': self.entity_id,
            'race_id': self.race_id,
            'class_id': self.class_id,
            'level': self.level,
            'background_id': self.background_id,
            'alignment_id': self.alignment_id,
            'faith': self.faith,
            'feat_id': self.feat_id,
            'alignment': self.npc_alignment.alignment,
            'background': self.npc_background.background,
            '_class': self.npc_class.ent_class,
            'entity': self.entity.serialize,
            'feat': self.npc_feat.feat,
            'race': self.npc_race.race
        }


class Race(Base):
    __tablename__ = 'races'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('races_id_seq'::regclass)"))
    race = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from Race table.
        """
        return {
            'id': self.id,
            'race': self.race
        }


class Size(Base):
    __tablename__ = 'sizes'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('sizes_id_seq'::regclass)"))
    size = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from Size table.
        """
        return {
            'id': self.id,
            'size': self.size
        }


class SwarmType(Base):
    __tablename__ = 'swarm_types'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('swarm_types_id_seq'::regclass)"))
    swarm_type = Column(String(25))

    @property
    def serialize(self):
        """

        :return: Serialized data from SwarmType table.
        """
        return {
            'id': self.id,
            'swarm_type': self.swarm_type
        }


class Turn(Base):
    __tablename__ = 'turns'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('turns_id_seq'::regclass)"))
    battle_id = Column(ForeignKey(u'battles.id'), nullable=False)
    attacker_id = Column(ForeignKey(u'entities.id'), nullable=False)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))

    actions = relationship(u'Action')
    entity = relationship(u'Entity')
    battle = relationship(u'Battle')

    @property
    def serialize(self):
        """

        :return: Serialized data from Turn table.
        """
        return {
            'id': self.id,
            'battle_id': self.battle_id,
            'attacker_id': self.attacker_id,
            'attacker_name': self.entity.name,
            'created_dt': self.created_dt,
            'battle_name': self.battle.name,
            'actions': [a.serialize for a in self.actions]
        }


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('users_id_seq'::regclass)"))
    email = Column(String(255), nullable=False, unique=True)
    role_id = Column(ForeignKey(u'user_roles.id'), nullable=False)
    password = Column(LargeBinary, nullable=False)
    created_dt = Column(DateTime(True), server_default=text(
        "CURRENT_TIMESTAMP"))

    user_role = relationship(u'UserRole')

    @property
    def serialize(self):
        """

        :return: Serialized data from User table.
        """
        return {
            'id': self.id,
            'email': self.email,
            'role_id': self.role_id,
            'created_dt': self.created_dt,
            'role': self.user_role.role
        }


class UserRole(Base):
    __tablename__ = 'user_roles'

    id = Column(Integer, primary_key=True, server_default=text(
        "nextval('user_roles_id_seq'::regclass)"))
    role = Column(String(20))

    @property
    def serialize(self):
        """

        :return: Serialized data from UserRole table.
        """
        return {
            'id': self.id,
            'role': self.role
        }

#!/bin/env python
# coding: utf-8
"""
This module is a simple testing framework for our restful API.
"""
import requests
import os
import re
from sqlalchemy import create_engine

__author__ = "Harry Staley <staleyh@craftedtech.net>"
__version__ = "1.0"

# Host
ip_addr = "http://paladinsinpurgatory.ddns.net"

# Filepath
base_folder = os.path.dirname(__file__)
file_name = 'REST_Service.py'
py_file = os.path.join(base_folder, file_name)
test_file = 'REST_TEST_SAMPLES.json'

# Global Counter
get_fail_count = 0
get_pass_count = 0
get_skip_count = 0
post_fail_count = 0
post_pass_count = 0
post_skip_count = 0

# Lists
failed_get_requests = []
failed_post_requests = []
urls = []
get_requests = []
post_requests = []
request_urls = []
campaign_ids = []
battle_ids = []
monster_ids = []
npc_ids = []
character_ids = []
entity_ids = []
turn_ids = []
user_ids = []
queries = ("SELECT id FROM campaigns",
           "SELECT id FROM battles",
           "SELECT entity_id FROM monsters",
           "SELECT entity_id FROM npcs",
           "SELECT entity_id FROM characters",
           "SELECT user_id FROM entities",
           "SELECT turn_id FROM actions",
           "SELECT id FROM users"
           )

# Database
db_uri = 'postgresql:///battle_tome'
db = create_engine(db_uri)


# Main
def main():
    """

    :return: Runs print_header() and test_api() methods.
    """
    print_header()
    test_api(py_file)


# Print Header Function
def print_header():
    """

    :return: The header of the api results.
    """
    print("++++++++++++++++++++++++++++++")
    print("         API Test App")
    print("++++++++++++++++++++++++++++++")
    print()


# Read File Function
def test_api(py_file):
    """

    :param py_file: This is the flask python file that is to be tested.
    :type py_file: String
    :return: The output of the tests run to a file.
    """

    # Read file line by line
    print("------------------------------")
    print("Start of API Test...")
    print("------------------------------")

    # Get IDs for id records through SQL Queries
    print("Running SQL Queries...")
    print("------------------------------")
    for query in queries:
        result_set = db.execute(query)
        results = [r[0] for r in result_set]
        for res in results:
            if query.find("campaigns") >= 0:
                campaign_ids.append(res)
            elif query.find("battles") >= 0:
                battle_ids.append(res)
            elif query.find("MON") >= 0:
                monster_ids.append(res)
            elif query.find("NPC") >= 0:
                npc_ids.append(res)
            elif query.find("CHAR") >= 0:
                character_ids.append(res)
            elif query.find("users") >= 0:
                user_ids.append(res)
    print("Finished SQL Queries...")
    print("------------------------------")

    # Open the code File and Parse
    with open(py_file) as f:
        line = f.readline()

        # Counters
        global post_skip_count
        cnt = 0
        get_cnt = 0
        post_cnt = 0

        # Main Logic
        while line:
            if line.find("@app.route") >= 0:

                # Append to GET and POST Lists
                if line.find("GET") >= 0:
                    get_requests.append(True)
                else:
                    get_requests.append(False)
                if line.find("POST") >= 0:
                    post_requests.append(True)
                else:
                    post_requests.append(False)

                # Append to URL List
                api_path = line[line.find("('") + 2:line.find("',")]
                full_url = "{}{}".format(ip_addr, api_path)
                urls.append(full_url)
            line = f.readline()

    request_urls.append(get_requests)
    request_urls.append(post_requests)
    request_urls.append(urls)

    for i in range(len(request_urls[0])):
        cnt += 1
        if request_urls[2][i].find(
                "<int:campaign_id>") >= 0 and request_urls[2][i].find(
                "<int:battle_id>") >= 0:
            for campaign in campaign_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:campaign_id"):request_urls[2][i].find(
                        ">") + 1], str(campaign))
                if request_urls[2][i].find("<int") >= 0:
                    for battle in battle_ids:
                        url = request_urls[2][i].replace(
                            request_urls[2][i][request_urls[2][i].find(
                                "<int"):request_urls[2][i].find(
                                ">") + 1], str(battle))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:campaign_id>") >= 0:
            for campaign in campaign_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:campaign_id"): request_urls[2][i].find(
                        ">") + 1], str(campaign))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:battle_id>") >= 0:
            for battle in battle_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:battle_id"): request_urls[2][i].find(
                        ">") + 1], str(battle))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:monster_id>") >= 0:
            for monster in monster_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:monster_id"):request_urls[2][i].find(
                        ">") + 1], str(monster))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:npc_id>") >= 0:
            for npc in npc_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:npc_id"):request_urls[2][i].find(
                        ">") + 1], str(npc))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:character_id>") >= 0:
            for character in character_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:character_id"):request_urls[2][i].find(
                        ">") + 1], str(character))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:entity_id>") >= 0:
            for entity in entity_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:entity_id"):request_urls[2][i].find(
                        ">") + 1], str(entity))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:turn_id>") >= 0:
            for turn in turn_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:turn_id"):request_urls[2][i].find(
                        ">") + 1], str(turn))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        elif request_urls[2][i].find("<int:user_id>") >= 0:
            for user in user_ids:
                url = request_urls[2][i].replace(
                    request_urls[2][i][request_urls[2][i].find(
                        "<int:user_id"):request_urls[2][i].find(
                        ">") + 1], str(user))
                if request_urls[0][i]:
                    get_cnt += 1
                    test_get(url)
                if request_urls[1][i]:
                    post_cnt += 1
                    test_post(url, test_file)
        else:
            if request_urls[0][i]:
                get_cnt += 1
                test_get(request_urls[2][i])
            if request_urls[1][i]:
                if not request_urls[2][i].find("pdf") >= 0:
                    post_cnt += 1
                    test_post(request_urls[2][i], test_file)
                else:
                    post_skip_count += 1
    print()
    print("------------------------------")
    print("            REPORT")
    print("------------------------------")
    print("Number of URLs: {}".format(cnt))
    print("GET Requests tested: {}".format(get_cnt))
    print("POST Requests tested: {}".format(post_cnt))
    print("------------------------------")
    print("         GET REQUESTS")
    print("------------------------------")
    print("Passed : {}".format(get_pass_count))
    print("Failed : {}".format(get_fail_count))
    print("Skipped: {}".format(get_skip_count))
    print()
    print("Percentage passed: {0:.0%}".format(get_pass_count/(
            get_pass_count + get_fail_count)))
    print()
    print("Failed GET Requests:")
    for i in failed_get_requests:
        print(i)
    print()
    print("------------------------------")
    print("        POST  REQUESTS")
    print("------------------------------")
    print("Passed : {}".format(post_pass_count))
    print("Failed : {}".format(post_fail_count))
    print("Skipped: {}".format(post_skip_count))
    print()
    print("Percentage passed: {0:.0%}".format(post_pass_count/(
            post_pass_count + post_fail_count)))
    print()
    print("Failed POST Requests:")
    for i in failed_post_requests:
        print(i)
    print()
    print("------------------------------")
    print("End of API Test...")
    print("------------------------------")


def test_post(uri, test_file):
    """

    :param uri: This is the location of the API endpoint that you want to test.
    :type uri: String
    :param test_file: This is the name of the file that contains the JSON
     data needed for the POST tests for the API.
    :type test_file: String
    :return: The URL tested, HTTP Request type, and the success or failure of
     the test.
    """
    pattern = "20*"
    resp = requests.post(uri, data=open(test_file, 'rb'))

    if re.match(pattern, str(resp.status_code)):
        print('\nURL: {}\nRequest: POST\nStatus: PASSED!'.format(uri))
        global post_pass_count
        post_pass_count += 1
    else:
        print('\nURL: {}\nRequest: POST\nStatus: FAILED!'.format(uri))
        failed_post_requests.append(uri)
        global post_fail_count
        post_fail_count += 1


def test_get(uri):
    """

    :param uri: This is the location of the API endpoint that you want to test.
    :type uri: String
    :return: uri tested | request type | status
    """
    pattern = "20*"
    resp = requests.get(uri)

    if re.match(pattern, str(resp.status_code)):
        print('\nURL: {}\nRequest: GET\nStatus: PASSED!'.format(uri))
        global get_pass_count
        get_pass_count += 1
    else:
        print('\nURL: {}\nRequest: GET\nStatus: FAILED!'.format(uri))
        failed_get_requests.append(uri)
        global get_fail_count
        get_fail_count += 1


if __name__ == '__main__':
    main()

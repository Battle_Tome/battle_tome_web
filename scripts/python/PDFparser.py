#!/bin/env python

import sys

import PyPDF2


def parse_char_pdf(src=sys.argv[1]):
    """
    This method parses a PDF file from D and D Beyond and parses it.
    :param src:
    :return json with parsed data.:
    """
    input1 = PyPDF2.PdfFileReader(open(src, "rb"))
    nPages = input1.getNumPages()
    attribute_list = {'ALIGNMENT': 'Alignment', 'FAITH': 'Faith',
                      'CharacterName': 'CharacterName',
                      'CLASS  LEVEL': ['Class', 'Lvl'], 'RACE': 'Race',
                      'BACKGROUND': 'Background',
                      'STRmod': 'Strength', 'DEXmod ': 'Dexterity',
                      'CONmod': 'Constitution', 'INTmod': 'Intelligence',
                      'WISmod': 'Wisdom', 'CHamod': 'Charisma'}
    data = {}
    with open('attr.txt', 'w') as t:
        for i in range(nPages):
            page0 = input1.getPage(i)
            try:
                for annot in page0['/Annots']:
                    temp_attr = annot.getObject()['/T']
                    if temp_attr in attribute_list.keys():
                        if temp_attr == 'CLASS  LEVEL':
                            temp = annot.getObject()['/V'].split(' ')
                            data[attribute_list[
                                'CLASS  LEVEL'][-1]] = temp[-1]
                            data[attribute_list[
                                'CLASS  LEVEL'][0]] = ' '.join(temp[0:-1])
                        else:
                            data[attribute_list[
                                temp_attr]] = annot.getObject()['/V']

            except Exception as e:
                print(e)
            finally:
                pass
        for key, val in data.items():
            t.write(key + ' : ' + val + '\n')


parse_char_pdf()

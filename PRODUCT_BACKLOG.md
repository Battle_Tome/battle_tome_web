# Battle Tome Product Backlog

This is where we are tracking features that are suggested by users and developers onf the product, but that are either slated for a later release.

## Character Generator
- Add XP field to the background field set.


## Character Generator

Add the following fields to the character form including validation.

- Add languages to the background field set.
	required field with multi select combobox list with options for all languages with the ability to add and delete languages from the list. 
- Add skills to the background field set.
	required field with select combobox list with options for all skills 
- Add custom background as a seleciton for background choices as well as a subform to handle this.
- Add lifestyle combobox to "background"
- Add physical characteristics field set containing the following fields.
	- Hair
		Textbox with upper and lower alphaabet only and max length of 20 characters.
	- Skin
		Textbox with upper and lower alphaabet only and max length of 20 characters.
	- Eyes
		Textbox with upper and lower alphaabet only and max length of 20 characters.
	- Height
		Textbox with no special chars and max length of 10 characters
	- Weight (lbs) 
		Textbox with positive integer validation and max length of 10 characters
	- Age (Years)
		Textbox with positive integer validation and max length of 10 characters
	- Gender
		radio buttons with M/F options.
- Add personal characteristics field set
	- TRAITS
	- IDEALS
	- BONDS
	- FLAWS
- Add notes fieldset
	- ORGANIZATIONS
	- ALLIES
	- ENEMIES
	- BACKSTORY
	- OTHER
- Add additional validations to further enforce game rules that change based upon the options selected given the added character data points.
- Set Monsters to have a default alignment that is defined by the Monster Manual, but that allows the user to change it as need be
- Change lvl type in JSON to integer from string for future calculations

### Other Generators
- City/Town/Village Generator
  - Population
  - Races
  - Type of Governance
  - etc
- Tavern Generator
  - List of available drinks
  - Mini-games
  - Persons of Interest
- Combat Simulation
  - Test to see if your characters survive against a proposed combat situation you build
- Map Generator
  - Create dungeon maps

### Security Concerns
- NGINX permissions are set to root, I would prefer them to www-data
- Postgres permissions on paladinsinhell.socket that allows users to query the backend API, this account needs to be read-only
  - User accounts need to match on both nginx and the socket, which means there has to be an account for postgres that is read-only and one for reading index files

### Fixes
1. Fixed condition button position on the small screen
2. Fixed target list item size on the small screen
3. Fixed condition list item size on the small screen
4. Fixed log list item delete button styles

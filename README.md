# Battle Tome: Web

The goal of the Battle Tome is to solve the problem of keeping track of your epic battles and logging characters prowess in a battle which many dungeon masters have a difficult time tracking and thus are not used in future sessions.  Dungeon Masters (also known as Game Moderators) have the inconvenient responsibility of keeping track of a plethora of rules and mechanisms that often get murky because certain rules are forgotten or misinterpreted, and even worse, not tracked. The problem this creates is inconsistent games where characters are not being tracked correctly, statuses and conditions are overlooked, and the essence of “what happened” is lost to memory.

This application will solve a huge need of keeping track of battles from one sessions to another, automate the mechanisms of battle, and log them for future reference. This application will lessen the burden on the Dungeon Master allowing them to focus more on the things we all love... Roleplaying, the story, comradeary, and most of all looting dungeons.

## Getting Started

## Built With
- VueJS (Frontend)
- HTML5/CSS3
- Python (Backend)
- Ubuntu (Host OS)
- Amazon AWS and Digital Ocean (Hosting)
- NGINX (Web Server)
- Let's Encrypt (For HTTPS)
- Flask (Web Framework)
- Gunicorn (Web Server for API)
- PostgreSQL
- RESTful API (Using PostgreSQL)
- GitLab CI/CD (For Continuous Integration and Delivery)
- Bootstrap and JQuery (For GitLab Pages generated from CI/CD)
- Bash and Ansible (For scripting, building concurrently with Pipelines)
- Vagrant (For test environment using custom Vagrantfile)

## Instructions to Create Development Environment for Testing/Development
- Please refer to [README](https://gitlab.com/Battle_Tome/battle_tome_web/tree/development/scripts/bash) for it

## Versioning

We use [SemVer](https://semver.org/spec/v2.0.0.html) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/Battle_Tome/battle_tome_android). 

## Authors

* **Artem Skitenko** - [artemskitenko93](https://gitlab.com/artemskitenko93)
* **Harry Staley** - [harrystaley](https://gitlab.com/harrystaley)
* **David Velez** - [dveleztx](https://gitlab.com/dveleztx)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Wizards of the Coast
* Tyler Duzan
* The fans of role playing games everywhere.

## Support

For support please submit your bug report to our [gitlab issue tracker](https://gitlab.com/Battle_Tome/battle_tome_android/issues).

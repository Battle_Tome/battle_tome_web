# Campaigns

Component that fetches and displays the list of campaigns

## Methods

<!-- @vuese:Campaigns:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|goToBattles|Used to redirect user to a list of battles for the specific campaign|The argument is a string value representing campaign id|

<!-- @vuese:Campaigns:methods:end -->



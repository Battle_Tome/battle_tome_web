# Characters

This veiw displays the list of all characters

## Methods

<!-- @vuese:Characters:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|searchCharacter|Used to filter characters by their names.|The argument is a string that will be matched to the character sequence in each character's name.|

<!-- @vuese:Characters:methods:end -->



# Initiative

Component that keeps track of the initiative order

## Props

<!-- @vuese:Initiative:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|campaignIndex|Index of the current campaign|`String`|`true`|-|
|battleIndex|Index of the current battle|`String`|`true`|-|

<!-- @vuese:Initiative:props:end -->



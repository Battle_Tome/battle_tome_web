# Activity

Component that provides action-logging functionality

## Props

<!-- @vuese:Activity:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|campaignIndex|Index of the current campaign|`String`|`true`|-|
|battleIndex|Index of the current battle|`String`|`true`|-|

<!-- @vuese:Activity:props:end -->


## Methods

<!-- @vuese:Activity:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|parseToText|Used to translate JSON data about the submitted action into a human readable sentence.|The argument is an object representing the information collected about the submitted  action. For example, targeted foes and allies, damage and damage type, condition etc.|
|subActionCreate|Used to create a subaction inside the condition dialog.|The argument is a boolean value. True for the function call when the used clicked the "Condition" button, False for any other function call.|
|miss|Used to log a miss for the current action.|-|
|submit|Used to prepare and send data for the current turn to the server and save actions for the current turn.|-|
|clearSelectedSpell|Used to clear the values in the spells dropdown and spell level input field.|-|
|clear|Used to reset the form.|-|
|checkAll|Used to assign boolean value of the second argument to the property field named "selected" for all characters or foes.|The first argument is an array of characters or foes; The second argumet is a boolean value that will be assigned to the property field named "selected" for all characters or foes.|
|charactersCondition|Used to filter and return only selected characters or foes.|The argument is an array of characters or foes.|
|searchTarget|Used to filter characters and foes by their names.|The argument is a string that will be matched to the character sequence in each character's and foe's name.|
|deleteSubAction|Used to delete a subaction by it's index.|The argument is a string value of the index.|
|deleteLogEntry|Used to delete a log entry by it's id.|The argument is a string value of the id.|
|copyData|Utility function for making a deep copy of an object.|The argument is an object to be copied.|
|addLogEntry|Used to add a log entry of the current action.|-|

<!-- @vuese:Activity:methods:end -->


## Computed

<!-- @vuese:Activity:computed:start -->
|Computed|Description|
|---|---|
|showSubmit|Returns boolean value to toggle disabled property of the "Submit" button.|
|hasSelectedCharacters|Returns an array of selected characters.|
|hasSelectedFoes|Returns an array of selected foes.|
|spellLevelErrors|Returns an array of input validation errors related to the spell level.|
|spellErrors|Returns an array of input validation errors related to the spells.|
|actionErrors|Returns an array of input validation errors related to the action type.|

<!-- @vuese:Activity:computed:end -->


## MixIns

<!-- @vuese:Activity:mixIns:start -->
|MixIn|
|---|
|validationMixin|

<!-- @vuese:Activity:mixIns:end -->



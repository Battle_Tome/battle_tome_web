# Monsters

This veiw displays the list of all monsters

## Methods

<!-- @vuese:Monsters:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|searchMonster|Used to filter monsters by their names.|The argument is a string that will be matched to the character sequence in each monster's name.|

<!-- @vuese:Monsters:methods:end -->



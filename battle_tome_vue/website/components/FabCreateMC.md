# FabCreateMC

Component that fetches and displays the list of campaigns

## Props

<!-- @vuese:FabCreateMC:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|create_mc_type|String with the type of the entity|`String`|`true`|-|

<!-- @vuese:FabCreateMC:props:end -->


## Methods

<!-- @vuese:FabCreateMC:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|navigateTo|Used to redirect the user to the route passed as the argument.|The argument is a string value the route.|
|fabClick|Used to log FAB click event to the console (in developmet and testing).|The argument is a string with the type of the entity passed in the prop.|

<!-- @vuese:FabCreateMC:methods:end -->



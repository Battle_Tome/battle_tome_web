# FabCreatePC

Component that fetches and displays the list of campaigns

## Props

<!-- @vuese:FabCreatePC:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|create_pc_type|String with the type of the entity|`String`|`true`|-|

<!-- @vuese:FabCreatePC:props:end -->


## Methods

<!-- @vuese:FabCreatePC:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|navigateTo|Used to redirect the user to the route passed as the argument.|The argument is a string value the route.|
|fabClick|Used to open a file selector popup.|-|
|pdfRead|Used to upload the PDF file to the server.|The argument is an onChange event of the HTML form.|

<!-- @vuese:FabCreatePC:methods:end -->



# Character

This veiw displays character creation page

## Methods

<!-- @vuese:Character:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|submit|Used to send data for the character sheet to the server.|-|

<!-- @vuese:Character:methods:end -->


## Computed

<!-- @vuese:Character:computed:start -->
|Computed|Description|
|---|---|
|backgroundErrors|Returns an array of input validation errors related to the background.|
|alignmentErrors|Returns an array of input validation errors related to the alignment.|
|classErrors|Returns an array of input validation errors related to the class.|
|raceErrors|Returns an array of input validation errors related to the race.|
|faithErrors|Returns an array of input validation errors related to the faith.|
|nameErrors|Returns an array of input validation errors related to the name of a character.|
|strengthErrors|Returns an array of input validation errors related to strength.|
|dexterityErrors|Returns an array of input validation errors related to dexterity.|
|intelligenceErrors|Returns an array of input validation errors related to intelligence.|
|constitutionErrors|Returns an array of input validation errors related to constitution.|
|wisdomErrors|Returns an array of input validation errors related to wisdom.|
|charismaErrors|Returns an array of input validation errors related to the charisma.|

<!-- @vuese:Character:computed:end -->


## MixIns

<!-- @vuese:Character:mixIns:start -->
|MixIn|
|---|
|validationMixin|

<!-- @vuese:Character:mixIns:end -->



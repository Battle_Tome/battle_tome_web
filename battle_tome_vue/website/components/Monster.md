# Monster

This veiw displays monster creation page

## Methods

<!-- @vuese:Monster:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|submit|Used to send monsters stats to the server.|-|

<!-- @vuese:Monster:methods:end -->


## Computed

<!-- @vuese:Monster:computed:start -->
|Computed|Description|
|---|---|
|armorClsErrors|Returns an array of input validation errors related to the armor class.|
|armorClsTypeErrors|Returns an array of input validation errors related to the armor class type.|
|alignmentErrors|Returns an array of input validation errors related to the alignment.|
|attDexterityErrors|Returns an array of input validation errors related to dexterity.|
|attIntelligenceErrors|Returns an array of input validation errors related to intelligence.|
|attStrengthErrors|Returns an array of input validation errors related to strength.|
|attAgilityErrors|Returns an array of input validation errors related to agility.|
|attConstitutionErrors|Returns an array of input validation errors related to constitution.|
|attWisdomErrors|Returns an array of input validation errors related to wisdom.|
|attCharismaErrors|Returns an array of input validation errors related to charisma.|
|challengeErrors|Returns an array of input validation errors related to challenge rating.|
|classErrors|Returns an array of input validation errors related to class.|
|faithErrors|Returns an array of input validation errors related to faith.|
|nameErrors|Returns an array of input validation errors related to the name of a monster.|
|sizeErrors|Returns an array of input validation errors related to size.|
|typeErrors|Returns an array of input validation errors related to type.|

<!-- @vuese:Monster:computed:end -->


## MixIns

<!-- @vuese:Monster:mixIns:start -->
|MixIn|
|---|
|validationMixin|

<!-- @vuese:Monster:mixIns:end -->



# Battles-and-Characters

This veiw displays the list of battles and characters in current campaign

## Methods

<!-- @vuese:Battles-and-Characters:methods:start -->
|Method|Description|Parameters|
|---|---|---|
|createDefaults|Used to assign False value to each monster's and charachter's property named "selected".|-|
|addBattle|Used to create a new battle and route the user to the combat.|-|
|goToCombat|Used to route the user to the combat based on the campaign and battle ids.|The first argument is a string value of the campaign id; The second argumet is a string value of the battle id.|
|goToCharacterCreate|Used to route the user to the character creation page.|-|
|checkAll|Used to assign boolean value of the second argument to the property field named "selected" for all characters or foes.|The first argument is an array of characters or foes; The second argumet is a boolean value that will be assigned to the property field named "selected" for all characters or foes.|
|combatantsChecked|Used to filter selected characters and monsters|The first argument is an array of characters; The second argumet is an array of monsters.|
|verifyAllChars|Used to check if all characters are selected and write the result to 'this.select_all_characters'.|-|
|verifyAllMons|Used to check if all monsters are selected and write the result to 'this.select_all_monsters'.|-|

<!-- @vuese:Battles-and-Characters:methods:end -->


## Computed

<!-- @vuese:Battles-and-Characters:computed:start -->
|Computed|Description|
|---|---|
|nameErrors|Returns an array of input validation errors related to the name of a battle.|

<!-- @vuese:Battles-and-Characters:computed:end -->


## MixIns

<!-- @vuese:Battles-and-Characters:mixIns:start -->
|MixIn|
|---|
|validationMixin|

<!-- @vuese:Battles-and-Characters:mixIns:end -->



// Developer: Artem Skitenko
// Modified: Harry Staley
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/combat/campaignIndex=:campaignId&battleIndex=:battleId',
      name: 'combat',
      component: () => import('./views/Combat.vue')
    },
    {
      path: '/battles-and-characters/campaignIndex=:campaignId',
      name: 'battles-and-characters',
      component: () => import('./views/Battles-and-Characters.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue')
    },
    {
      path: '/monsters',
      name: 'monsters',
      component: () => import('./views/Monsters.vue')
    },
    {
      path: '/monsters/create',
      name: 'createMonster',
      component: () => import('./views/Monster.vue')
    },
    {
      path: '/monsters/display/id=:id', // ?monsterIndex=:monsterId
      name: 'displayMonster',
      component: () => import('./views/Monster.vue')
    },
    {
      path: '/monsters/edit/id=:id', // ?monsterIndex=:monsterId
      name: 'editMonster',
      component: () => import('./views/Monster.vue')
    },
    {
      path: '/characters',
      name: 'characters',
      component: () => import('./views/Characters.vue')
    },
    {
      path: '/characters/create',
      name: 'createCharacter',
      component: () => import('./views/Character.vue')
    },
    {
      path: '/characters/edit/id=:id',
      name: 'editCharacter',
      component: () => import('./views/Character.vue')
    },
    {
      path: '/characters/display/id=:id', // ?characterIndex=:characterId
      name: 'displayCharacter',
      component: () => import('./views/Character.vue')
    },
    {
      path: '/help',
      name: 'help',
      component: () => import('./views/Help.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login-Registration.vue')
    },
    {
      path: '/contact',
      name: 'contact',
      component: () => import('./views/Contact.vue')
    },
    {
      path: '/404',
      name: 'notFound',
      component: () => import('./views/NotFound.vue')
    },
    {
      path: '*',
      redirect: '/404'
    }
  ]
})

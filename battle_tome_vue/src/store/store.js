import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  strict: true,
  state: {
    currentTurn: NaN,
    currentTurnName: '',
    initiativeOrder: [],
    turnQueue: [],
    turnEntityId: '',
    battleEntities: [],
    turns: []
  },

  getters: {
    currentTurn: state => state.currentTurn,
    turnQueue: state => state.turnQueue,
    initiativeOrder: state => state.initiativeOrder,
    currentTurnName: state => state.currentTurnName,
    turnEntityId: state => state.turnEntityId,
    battleEntities: state => state.battleEntities,
    turns: state => state.turns
  },
  mutations: {
    setCurrentTurn: (state, payload) => {
      state.currentTurn = payload
    },
    setTurnQueue: (state, payload) => {
      state.turnQueue = payload
    },
    addToTurnQueue: (state, payload) => {
      state.turnQueue.unshift(payload)
    },
    setInitiativeOrder: (state, payload) => {
      state.initiativeOrder = payload
    },
    setCurrentTurnName: (state, payload) => {
      state.currentTurnName = payload
    },
    setTurnEntityId: (state, payload) => {
      state.turnEntityId = payload
    },
    setBattleEntities: (state, payload) => {
      state.battleEntities = payload
    },
    setTurns: (state, payload) => {
      state.turns = payload
    },
    clearTurnQueue: (state) => {
      state.turnQueue = []
    }
  },
  actions: {
    setCurrentTurn: (context, payload) => {
      context.commit('setCurrentTurn', payload)
    },
    setTurnQueue: (context, payload) => {
      context.commit('setTurnQueue', payload)
    },
    addToTurnQueue: (context, payload) => {
      context.commit('addToTurnQueue', payload)
    },
    setInitiativeOrder: (context, payload) => {
      context.commit('setInitiativeOrder', payload)
    },
    setCurrentTurnName: (context, payload) => {
      context.commit('setCurrentTurnName', payload)
    },
    setTurnEntityId: (context, payload) => {
      context.commit('setTurnEntityId', payload)
    },
    setBattleEntities: (context, payload) => {
      context.commit('setBattleEntities', payload)
    },
    setTurns: (context, payload) => {
      context.commit('setTurns', payload)
    },
    clearTurnQueue: (context) => {
      context.commit('clearTurnQueue')
    }
  }
})
